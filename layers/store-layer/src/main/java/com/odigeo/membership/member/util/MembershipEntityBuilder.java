package com.odigeo.membership.member.util;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.enums.db.MembershipField;
import com.odigeo.membership.parameters.search.MembershipSearch;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Singleton
public class MembershipEntityBuilder {

    public static final String AUTO_RENEWAL_DISABLED = MembershipRenewal.DISABLED.toString();
    public static final String AUTO_RENEWAL_ENABLED = MembershipRenewal.ENABLED.toString();

    public Membership build(ResultSet rs, boolean withAccount) throws SQLException {
        MembershipBuilder membershipBuilder = new MembershipBuilder();
        fillCommonFields(rs, membershipBuilder, withAccount);
        return membershipBuilder.build();
    }

    public List<Membership> buildListFromSearch(MembershipSearch membershipSearch, ResultSet resultSet) throws SQLException {
        List<Membership> memberships = new ArrayList<>();
        if (membershipSearch.isWithStatusActions()) {
            memberships = buildMembershipWithStatusActions(resultSet, membershipSearch.isWithMemberAccount());
        } else {
            while (resultSet.next()) {
                memberships.add(build(resultSet, membershipSearch.isWithMemberAccount()));
            }
        }
        return memberships;
    }

    public List<Membership> buildListFromResultSet(ResultSet rs) throws SQLException {
        List<Membership> membershipList = new ArrayList<>();
        while (rs.next()) {
            membershipList.add(build(rs, false));
        }
        return membershipList;
    }

    private List<Membership> buildMembershipWithStatusActions(ResultSet rs, boolean withMemberAccount) throws SQLException {
        Map<Long, Membership> membershipMap = new LinkedHashMap<>();
        while (rs.next()) {
            Long membershipId = rs.getLong(MembershipField.ID.name());
            membershipMap.putIfAbsent(membershipId, build(rs, withMemberAccount));
            membershipMap.get(membershipId).getMemberStatusActions()
                    .add(getStatusActionEntityBuilder().build(rs));
        }
        return new ArrayList<>(membershipMap.values());
    }

    private void fillCommonFields(ResultSet rs, MembershipBuilder membershipBuilder, boolean withAccount) throws SQLException {
        Long membershipId = Long.valueOf(rs.getString("ID"));
        String website = rs.getString("WEBSITE");
        MemberStatus status = MemberStatus.valueOf(rs.getString("STATUS"));
        MembershipRenewal membershipRenewal = MembershipRenewal.valueOf(rs.getString("AUTO_RENEWAL"));
        long memberAccountId = rs.getLong("MEMBER_ACCOUNT_ID");
        Timestamp activationDate = rs.getTimestamp("ACTIVATION_DATE");
        Timestamp expirationDate = rs.getTimestamp("EXPIRATION_DATE");
        BigDecimal balance = rs.getBigDecimal("BALANCE");
        MembershipType membershipType = MembershipType.valueOf(rs.getString("MEMBERSHIP_TYPE"));
        SourceType sourceType = SourceType.getNullableValue(rs.getString("SOURCE_TYPE"));
        int monthsDuration = rs.getInt("MONTHS_DURATION");
        String prStatus = rs.getString("PRODUCT_STATUS");
        ProductStatus productStatus = prStatus == null ? null : ProductStatus.valueOf(prStatus);
        String recurringId = rs.getString("RECURRING_ID");
        Timestamp timestamp = rs.getTimestamp("MEMBERSHIP_TIMESTAMP");
        String currencyCode = rs.getString("CURRENCY_CODE");
        BigDecimal totalPrice = rs.getBigDecimal("TOTAL_PRICE");
        BigDecimal renewalPrice = rs.getBigDecimal("RENEWAL_PRICE");
        MemberAccount memberAccount = null;
        if (withAccount) {
            memberAccount = getMemberAccountEntityBuilder().build(rs, "MEMBER_ACCOUNT_ID");
        }
        membershipBuilder.setId(membershipId).setWebsite(website).setStatus(status)
                .setMembershipRenewal(membershipRenewal).setMemberAccountId(memberAccountId)
                .setBalance(balance).setMembershipType(membershipType).setMonthsDuration(monthsDuration).setSourceType(sourceType)
                .setProductStatus(productStatus).setRecurringId(recurringId).setCurrencyCode(currencyCode).setTotalPrice(totalPrice)
                .setRenewalPrice(renewalPrice).setMemberStatusActions(new ArrayList<>()).setMemberAccount(memberAccount);
        setTimestamp(timestamp, membershipBuilder);
        setActivationDate(activationDate, membershipBuilder);
        setExpirationDate(expirationDate, membershipBuilder);
    }

    private static void setActivationDate(Timestamp activationDate, MembershipBuilder membershipBuilder) {
        if (Objects.nonNull(activationDate)) {
            membershipBuilder.setActivationDate(activationDate.toLocalDateTime());
        }
    }

    private static void setExpirationDate(Timestamp expirationDate, MembershipBuilder membershipBuilder) {
        if (Objects.nonNull(expirationDate)) {
            membershipBuilder.setExpirationDate(expirationDate.toLocalDateTime());
        }
    }

    private static void setTimestamp(final Timestamp timestamp, final MembershipBuilder membershipBuilder) {
        if (Objects.nonNull(timestamp)) {
            membershipBuilder.setTimestamp(timestamp.toLocalDateTime());
        }
    }

    private MemberAccountEntityBuilder getMemberAccountEntityBuilder() {
        return ConfigurationEngine.getInstance(MemberAccountEntityBuilder.class);
    }

    private StatusActionEntityBuilder getStatusActionEntityBuilder() {
        return ConfigurationEngine.getInstance(StatusActionEntityBuilder.class);
    }
}
