package com.odigeo.membership.member.util;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class MembershipEntityBuilderTest {

    private static final Long ID = 123L;
    private static final String WEBSITE = "ES";
    private static final String STATUS = "ACTIVATED";
    private static final String AUTO_RENEWAL = "ENABLED";
    private static final long MEMBER_ACCOUNT_ID_1 = 333L;
    private static final Date utilDate = new Date();
    private static final Timestamp ACTIVATION_TIMESTAMP = new Timestamp(utilDate.getTime());
    private static final Timestamp EXPIRATION_TIMESTAMP = new Timestamp(utilDate.getTime());
    private static final BigDecimal BALANCE = BigDecimal.TEN;
    private static final String MEMBERSHIP_TYPE = MembershipType.BASIC.toString();
    private static final String SOURCE_TYPE = SourceType.FUNNEL_BOOKING.toString();
    private static final int MONTHS_DURATION = 12;
    private static final String PRODUCT_STATUS = null;
    private static final String RECURRING_ID = null;
    private static final Timestamp MEMBERSHIP_TIMESTAMP = new Timestamp(utilDate.getTime());;
    private static final String CURRENCY_CODE = null;
    private static final BigDecimal TOTAL_PRICE = new BigDecimal(44.99);
    private static final BigDecimal RENEWAL_PRICE = new BigDecimal(45.99);


    @Mock
    private ResultSet resultSet;
    @Mock
    private MemberAccount memberAccount;
    @Mock
    private MemberAccountEntityBuilder memberAccountEntityBuilder;

    private MembershipEntityBuilder membershipEntityBuilder;

    @BeforeMethod
    public void before() {
        initMocks(this);
        ConfigurationEngine.init((binder) -> binder.bind(MemberAccountEntityBuilder.class).toInstance(memberAccountEntityBuilder));
        membershipEntityBuilder = new MembershipEntityBuilder();
    }

    @Test
    public void testBuildWithoutAccount() throws SQLException {
        // Given
        mockResultSet();
        // When
        Membership membership = membershipEntityBuilder.build(resultSet, false);
        // Then
        verifyMembershipFields(membership);
    }

    @Test
    public void testBuildWithAccount() throws SQLException {
        // Given
        mockResultSet();
        when(memberAccountEntityBuilder.build(resultSet, "MEMBER_ACCOUNT_ID")).thenReturn(memberAccount);
        // When
        Membership membership = membershipEntityBuilder.build(resultSet, true);
        // Then
        verifyMembershipFields(membership);
        assertEquals(membership.getMemberAccount(), memberAccount);
    }

    @Test
    public void testBuildWithStatusActions() throws SQLException {
        // Given
        mockResultSet();
        // When
        List<Membership> membershipList = membershipEntityBuilder.buildListFromResultSet(resultSet);
        // Then
        assertEquals(membershipList.size(), 2);
    }

    private void verifyMembershipFields(Membership membership) {
        assertEquals(membership.getId(), ID);
        assertEquals(membership.getWebsite(), WEBSITE);
        assertEquals(membership.getStatus(), MemberStatus.valueOf(STATUS));
        assertEquals(membership.getAutoRenewal(), MembershipRenewal.valueOf(AUTO_RENEWAL));
        assertEquals(membership.getMemberAccountId(), MEMBER_ACCOUNT_ID_1);
        assertEquals(membership.getExpirationDate(), EXPIRATION_TIMESTAMP.toLocalDateTime());
        assertEquals(membership.getActivationDate(), ACTIVATION_TIMESTAMP.toLocalDateTime());
        assertEquals(membership.getBalance(), BALANCE);
        assertEquals(membership.getMembershipType(), MembershipType.valueOf(MEMBERSHIP_TYPE));
        assertEquals(membership.getSourceType(), SourceType.valueOf(SOURCE_TYPE));
        assertEquals(membership.getMonthsDuration(), MONTHS_DURATION);
        assertNull(membership.getProductStatus());
        assertEquals(membership.getRecurringId(), RECURRING_ID);
        assertEquals(membership.getTimestamp(), MEMBERSHIP_TIMESTAMP.toLocalDateTime());
        assertEquals(membership.getCurrencyCode(), CURRENCY_CODE);
        assertEquals(membership.getTotalPrice(), TOTAL_PRICE);
        assertEquals(membership.getRenewalPrice(), RENEWAL_PRICE);
    }

    private void mockResultSet() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getString("ID")).thenReturn(String.valueOf(ID));
        when(resultSet.getString("WEBSITE")).thenReturn(WEBSITE);
        when(resultSet.getString("STATUS")).thenReturn(STATUS);
        when(resultSet.getString("AUTO_RENEWAL")).thenReturn(AUTO_RENEWAL);
        when(resultSet.getLong("MEMBER_ACCOUNT_ID")).thenReturn(MEMBER_ACCOUNT_ID_1);
        when(resultSet.getTimestamp("EXPIRATION_DATE")).thenReturn(EXPIRATION_TIMESTAMP);
        when(resultSet.getTimestamp("ACTIVATION_DATE")).thenReturn(ACTIVATION_TIMESTAMP);
        when(resultSet.getBigDecimal("BALANCE")).thenReturn(BALANCE);
        when(resultSet.getString("MEMBERSHIP_TYPE")).thenReturn(MEMBERSHIP_TYPE);
        when(resultSet.getString("SOURCE_TYPE")).thenReturn(SOURCE_TYPE);
        when(resultSet.getInt("MONTHS_DURATION")).thenReturn(MONTHS_DURATION);
        when(resultSet.getString("PRODUCT_STATUS")).thenReturn(PRODUCT_STATUS);
        when(resultSet.getString("RECURRING_ID")).thenReturn(RECURRING_ID);
        when(resultSet.getTimestamp("MEMBERSHIP_TIMESTAMP")).thenReturn(MEMBERSHIP_TIMESTAMP);
        when(resultSet.getString("CURRENCY_CODE")).thenReturn(CURRENCY_CODE);
        when(resultSet.getBigDecimal("TOTAL_PRICE")).thenReturn(TOTAL_PRICE);
        when(resultSet.getBigDecimal("RENEWAL_PRICE")).thenReturn(RENEWAL_PRICE);
    }
}