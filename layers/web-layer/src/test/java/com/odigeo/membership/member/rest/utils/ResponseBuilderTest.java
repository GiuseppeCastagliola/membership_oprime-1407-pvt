package com.odigeo.membership.member.rest.utils;

import com.odigeo.membership.cookies.Cookie;
import com.odigeo.membership.cookies.CookiesContext;
import org.testng.annotations.Test;

import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import java.time.LocalDateTime;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class ResponseBuilderTest {
    @Test
    public void buildResponseEntityTest() {
        Response response = ResponseBuilder.buildResponse(null, null);
        assertEquals(response.getStatus(), Response.Status.NO_CONTENT.getStatusCode());
        response = ResponseBuilder.buildResponse(new Entity(0), null);
        assertEquals(response.getStatus(), Response.Status.NO_CONTENT.getStatusCode());
        Request request = mock(Request.class);
        when(request.evaluatePreconditions(any(EntityTag.class))).thenReturn(null);
        response = ResponseBuilder.buildResponse(new Entity(1), request);
        assertEquals(response.getStatus(), Response.Status.OK.getStatusCode());
        assertEquals(response.getMetadata().get("ETag").get(0).toString(), "1");
        when(request.evaluatePreconditions(any(EntityTag.class))).thenReturn(Response.status(Response.Status.NOT_MODIFIED));
        when(request.getMethod()).thenReturn("GET");
        response = ResponseBuilder.buildResponse(new Entity(1), request);
        assertEquals(response.getStatus(), Response.Status.NOT_MODIFIED.getStatusCode());
        assertEquals(response.getMetadata().get("ETag").get(0).toString(), "1");
    }

    @Test
    public void buildResponseEntityTestWithCookies() {
        CookiesContext cookiesContext = prepareCookiesContext();
        Response response = ResponseBuilder.buildResponse(null, null, cookiesContext);
        assertEquals(response.getStatus(), Response.Status.NO_CONTENT.getStatusCode());
        response = ResponseBuilder.buildResponse(new Entity(0), null, cookiesContext);
        assertEquals(response.getStatus(), Response.Status.NO_CONTENT.getStatusCode());
        Request request = mock(Request.class);
        when(request.evaluatePreconditions(any(EntityTag.class))).thenReturn(null);
        response = ResponseBuilder.buildResponse(new Entity(1), request, cookiesContext);
        assertEquals(response.getStatus(), Response.Status.OK.getStatusCode());
        assertEquals(response.getMetadata().get("ETag").get(0).toString(), "1");
        when(request.evaluatePreconditions(any(EntityTag.class))).thenReturn(Response.status(Response.Status.NOT_MODIFIED));
        when(request.getMethod()).thenReturn("GET");
        response = ResponseBuilder.buildResponse(new Entity(1), request, cookiesContext);
        assertEquals(response.getStatus(), Response.Status.NOT_MODIFIED.getStatusCode());
        assertEquals(response.getMetadata().get("ETag").get(0).toString(), "1");
        when(request.getMethod()).thenReturn("POST");
        response = ResponseBuilder.buildResponse(new Entity(1), request, cookiesContext);
        assertEquals(response.getStatus(), Response.Status.CREATED.getStatusCode());
        assertEquals(response.getMetadata().get("ETag").get(0).toString(), "1");
    }

    private CookiesContext prepareCookiesContext() {
        Cookie cookie = new Cookie();
        cookie.setName("name");
        cookie.setValue("value");
        cookie.setPath("path");
        cookie.setDomain("domain");
        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDateTime localDateTimeModified = localDateTime.minusSeconds(100);
        cookie.setExpirationDate(localDateTimeModified);
        cookie.setSecure(false);
        Cookie cookie1 = new Cookie();
        cookie1.setName("name");
        cookie1.setValue("value");
        cookie1.setPath("path");
        cookie1.setDomain("domain");
        LocalDateTime localDateTime2 = LocalDateTime.now();
        LocalDateTime localDateTimeModified2 = localDateTime2.minusSeconds(100);
        cookie1.setExpirationDate(localDateTimeModified2);
        cookie1.setSecure(false);
        CookiesContext cookiesContext = new CookiesContext("outerDomain");
        cookiesContext.getStorage().addCookie(cookie);
        cookiesContext.getStorage().addCookie(cookie1);
        return cookiesContext;
    }

    private static final class Entity {
        private int intField;

        private Entity(final int intField) {
            this.intField = intField;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof Entity)) {
                return false;
            }
            final Entity entity = (Entity) o;
            return intField == entity.intField;
        }

        @Override
        public int hashCode() {
            return intField;
        }
    }
}