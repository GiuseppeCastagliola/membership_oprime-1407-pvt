package com.odigeo.membership.member.monitoring;

import com.codahale.metrics.health.HealthCheck;
import org.I0Itec.zkclient.exception.ZkException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class KafkaConsumerHealthCheckTest {

    private static final String TEST_URL = "fakeUrl";
    private static final String TEST_NAME = "testName";
    private static final String TEST_GROUP = "testGroup";
    private static final String TOPIC_OK = " topic ok: ";
    private static final String BROKERS_AVAILABLE = " brokers available";
    private static final int TEST_BROKERS_AVAILABLE = 1;

    private HealthCheck healthCheck;
    private KafkaConsumerHealthCheck kafkaConsumerHealthCheck;

    @BeforeMethod
    public void setUp() {
        this.kafkaConsumerHealthCheck = new KafkaConsumerHealthCheck(TEST_URL, TEST_NAME, TEST_GROUP);
    }

    @Test
    public void testConstructorAvailability() throws Exception {
        this.healthCheck = new KafkaConsumerHealthCheck(TEST_URL, TEST_NAME, TEST_GROUP);
        assertNotNull(healthCheck);
    }

    @Test(expectedExceptions = Exception.class)
    public void testGetConsumerAndConnectThrowsZkExceptionIfUnknownHost() {
        kafkaConsumerHealthCheck.getConsumerAndConnect(TEST_BROKERS_AVAILABLE);
    }

    @Test
    public void testGetResult() {
        final HealthCheck.Result result = kafkaConsumerHealthCheck.getResult(TEST_BROKERS_AVAILABLE);
        assertEquals(result.getMessage(), TEST_NAME + TOPIC_OK + TEST_BROKERS_AVAILABLE + BROKERS_AVAILABLE);
        assertTrue(result.isHealthy());
    }
}
