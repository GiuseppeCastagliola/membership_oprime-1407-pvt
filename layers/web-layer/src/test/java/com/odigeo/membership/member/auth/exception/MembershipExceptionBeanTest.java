package com.odigeo.membership.member.auth.exception;

import bean.test.BeanTest;

public class MembershipExceptionBeanTest extends BeanTest<MembershipExceptionBean> {

    private static final String ERROR = "ERROR";

    @Override
    protected MembershipExceptionBean getBean() {
        return new MembershipExceptionBean(MembershipAuthenticationException.class, ERROR);
    }
}