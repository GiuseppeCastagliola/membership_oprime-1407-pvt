package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.member.memberapi.v1.MembershipServiceController;
import com.odigeo.membership.member.memberapi.v1.SearchApiController;
import com.odigeo.membership.request.product.UpdateMemberAccountRequest;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.core.ResourceMethod;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.spi.HttpRequest;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipAutorenewalInterceptorTest {

    private static String UPDATE_MEMBERSHIP_URI = "http://localhost/membership/membership/v1/updateMembership/";
    private static String MEMBERSHIP_ENABLE_AUTORENEWAL_URI = "http://localhost/membership/membership/v1/enable-auto-renewal/22";
    private static String MEMBERSHIP_DISABLE_AUTORENEWAL_URI = "http://localhost/membership/membership/v1/disable-auto-renewal/22";
    private static String MEMBERSHIP_REACTIVATE_URI = "http://localhost/membership/membership/v1/reactivate/22";
    private static String MEMBERSHIP_DISABLE_URI = "http://localhost/membership/membership/v1/disable/22";

    private static String ENABLE_AUTO_RENEWAL_REQUEST_CONTENT = "{\n" +
            "  \"membershipId\": \"76754\",\n" +
            "  \"operation\": \"ENABLE_AUTO_RENEWAL\"\n" +
            "}";

    private static String DISABLE_AUTO_RENEWAL_REQUEST_CONTENT = "{\n" +
            "  \"membershipId\": \"76754\",\n" +
            "  \"operation\": \"DISABLE_AUTO_RENEWAL\"\n" +
            "}";

    private static String REACTIVATE_MEMBERSHIP_REQUEST_CONTENT = "{\n" +
            "  \"membershipId\": \"76754\",\n" +
            "  \"operation\": \"REACTIVATE_MEMBERSHIP\"\n" +
            "}";

    @Mock
    private ResourceMethod resourceMethodMock;

    @Captor
    ArgumentCaptor<HttpRequest> requestCaptor;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init();
    }

    @Test
    public void testAcceptUpdateMembershipMethod() throws NoSuchMethodException {
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = new MembershipAutorenewalInterceptor();
        Class<?> membershipServiceClass = MembershipServiceController.class.getInterfaces()[0];
        Method updateMembershipMethod = membershipServiceClass.getMethod("updateMembership", UpdateMembershipRequest.class);

        assertTrue(membershipAutorenewalInterceptor.accept(membershipServiceClass, updateMembershipMethod));
    }

    @Test
    public void testAcceptDisableAutorenewalMethod() throws NoSuchMethodException {
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = new MembershipAutorenewalInterceptor();
        Class<?> membershipServiceClass = MembershipServiceController.class.getInterfaces()[0];
        Method disableAutoRenewalMethod = membershipServiceClass.getMethod("disableAutoRenewal", Long.class);

        assertTrue(membershipAutorenewalInterceptor.accept(membershipServiceClass, disableAutoRenewalMethod));
    }

    @Test
    public void testAcceptEnableAutorenewalMethod() throws NoSuchMethodException {
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = new MembershipAutorenewalInterceptor();
        Class<?> membershipServiceClass = MembershipServiceController.class.getInterfaces()[0];
        Method disableAutoRenewalMethod = membershipServiceClass.getMethod("enableAutoRenewal", Long.class);

        assertTrue(membershipAutorenewalInterceptor.accept(membershipServiceClass, disableAutoRenewalMethod));
    }

    @Test
    public void testAcceptReactivateMembershipMethod() throws NoSuchMethodException {
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = new MembershipAutorenewalInterceptor();
        Class<?> membershipServiceClass = MembershipServiceController.class.getInterfaces()[0];
        Method disableAutoRenewalMethod = membershipServiceClass.getMethod("reactivateMembership", Long.class);

        assertTrue(membershipAutorenewalInterceptor.accept(membershipServiceClass, disableAutoRenewalMethod));
    }

    @Test
    public void testAcceptDisableMembershipMethod() throws NoSuchMethodException {
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = new MembershipAutorenewalInterceptor();
        Class<?> membershipServiceClass = MembershipServiceController.class.getInterfaces()[0];
        Method disableAutoRenewalMethod = membershipServiceClass.getMethod("disableMembership", Long.class);

        assertTrue(membershipAutorenewalInterceptor.accept(membershipServiceClass, disableAutoRenewalMethod));
    }

    @Test
    public void testAcceptNoAnyOtherMethod() throws NoSuchMethodException {
        Class<?> membershipServiceClass = MembershipServiceController.class.getInterfaces()[0];
        Method updateMemberAccountMethod = membershipServiceClass.getMethod("updateMemberAccount", UpdateMemberAccountRequest.class);
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = new MembershipAutorenewalInterceptor();

        assertFalse(membershipAutorenewalInterceptor.accept(membershipServiceClass, updateMemberAccountMethod));
    }

    @Test
    public void testAcceptNoAnyOtherClass() throws NoSuchMethodException {
        Class<?> membershipSearchApiClass = SearchApiController.class.getInterfaces()[0];
        Method searchAccountsMethod = membershipSearchApiClass.getMethod("searchAccounts", MemberAccountSearchRequest.class);
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = new MembershipAutorenewalInterceptor();

        assertFalse(membershipAutorenewalInterceptor.accept(membershipSearchApiClass, searchAccountsMethod));
    }

    @Test
    public void testPreProcessInvalidOperation() throws IOException, URISyntaxException {
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = spy(new MembershipAutorenewalInterceptor());
        HttpRequest httpRequestMock = MockHttpRequest.put(UPDATE_MEMBERSHIP_URI);
        httpRequestMock.setInputStream(new ByteArrayInputStream(REACTIVATE_MEMBERSHIP_REQUEST_CONTENT.getBytes()));
        InterceptorHelper interceptorHelper = spy(new InterceptorHelper());
        doReturn(interceptorHelper).when(membershipAutorenewalInterceptor).getInterceptorHelper();

        membershipAutorenewalInterceptor.preProcess(httpRequestMock, resourceMethodMock);

        verify(interceptorHelper, never()).extractClientModuleFromHeader(any());
        assertEquals(IOUtils.toByteArray(httpRequestMock.getInputStream()), REACTIVATE_MEMBERSHIP_REQUEST_CONTENT.getBytes());
    }

    @Test
    public void testPreProcessUpdateMembershipOperation() throws IOException, URISyntaxException {
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = spy(new MembershipAutorenewalInterceptor());
        HttpRequest httpRequestMock = MockHttpRequest.put(UPDATE_MEMBERSHIP_URI);
        httpRequestMock.setInputStream(new ByteArrayInputStream(ENABLE_AUTO_RENEWAL_REQUEST_CONTENT.getBytes()));
        InterceptorHelper interceptorHelper = spy(new InterceptorHelper());
        doReturn(interceptorHelper).when(membershipAutorenewalInterceptor).getInterceptorHelper();

        membershipAutorenewalInterceptor.preProcess(httpRequestMock, resourceMethodMock);

        verify(interceptorHelper).extractClientModuleFromHeader(any());
        assertEquals(IOUtils.toByteArray(httpRequestMock.getInputStream()), ENABLE_AUTO_RENEWAL_REQUEST_CONTENT.getBytes());
    }

    @Test
    public void testPreProcessUpdateMembershipDisableOperation() throws IOException, URISyntaxException {
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = spy(new MembershipAutorenewalInterceptor());
        HttpRequest httpRequestMock = MockHttpRequest.put(UPDATE_MEMBERSHIP_URI);
        httpRequestMock.setInputStream(new ByteArrayInputStream(DISABLE_AUTO_RENEWAL_REQUEST_CONTENT.getBytes()));
        InterceptorHelper interceptorHelper = spy(new InterceptorHelper());
        doReturn(interceptorHelper).when(membershipAutorenewalInterceptor).getInterceptorHelper();

        membershipAutorenewalInterceptor.preProcess(httpRequestMock, resourceMethodMock);

        verify(interceptorHelper).extractClientModuleFromHeader(any());
        assertEquals(IOUtils.toByteArray(httpRequestMock.getInputStream()), DISABLE_AUTO_RENEWAL_REQUEST_CONTENT.getBytes());
    }

    @Test
    public void testPreProcessEnableAutorenewalOperation() throws IOException, URISyntaxException {
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = spy(new MembershipAutorenewalInterceptor());
        HttpRequest httpRequestMock = MockHttpRequest
            .put(MEMBERSHIP_ENABLE_AUTORENEWAL_URI)
            .header("odigeo-module-info", "some-module:v1");
        httpRequestMock.setInputStream(new ByteArrayInputStream("".getBytes()));
        InterceptorHelper interceptorHelper = spy(new InterceptorHelper());
        doReturn(interceptorHelper).when(membershipAutorenewalInterceptor).getInterceptorHelper();

        membershipAutorenewalInterceptor.preProcess(httpRequestMock, resourceMethodMock);

        verify(interceptorHelper).extractClientModuleFromHeader(requestCaptor.capture());
        String clienModuleHeader = requestCaptor.getValue().getHttpHeaders().getRequestHeader("odigeo-module-info").get(0);
        assertEquals(clienModuleHeader, "some-module:v1");
        assertEquals(IOUtils.toByteArray(httpRequestMock.getInputStream()), "".getBytes());
    }

    @Test
    public void testPreProcessDisableAutorenewalOperation() throws IOException, URISyntaxException {
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = spy(new MembershipAutorenewalInterceptor());
        HttpRequest httpRequestMock = MockHttpRequest
            .put(MEMBERSHIP_DISABLE_AUTORENEWAL_URI)
            .header("odigeo-module-info", "some-module:v1");
        httpRequestMock.setInputStream(new ByteArrayInputStream("".getBytes()));
        InterceptorHelper interceptorHelper = spy(new InterceptorHelper());
        doReturn(interceptorHelper).when(membershipAutorenewalInterceptor).getInterceptorHelper();

        membershipAutorenewalInterceptor.preProcess(httpRequestMock, resourceMethodMock);

        verify(interceptorHelper).extractClientModuleFromHeader(requestCaptor.capture());
        String clienModuleHeader = requestCaptor.getValue().getHttpHeaders().getRequestHeader("odigeo-module-info").get(0);
        assertEquals(clienModuleHeader, "some-module:v1");
        assertEquals(IOUtils.toByteArray(httpRequestMock.getInputStream()), "".getBytes());
    }

    @Test
    public void testPreProcessMembershipReactivateOperation() throws IOException, URISyntaxException {
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = spy(new MembershipAutorenewalInterceptor());
        HttpRequest httpRequestMock = MockHttpRequest
                .put(MEMBERSHIP_REACTIVATE_URI)
                .header("odigeo-module-info", "some-module:v1");
        httpRequestMock.setInputStream(new ByteArrayInputStream("".getBytes()));
        InterceptorHelper interceptorHelper = spy(new InterceptorHelper());
        doReturn(interceptorHelper).when(membershipAutorenewalInterceptor).getInterceptorHelper();

        membershipAutorenewalInterceptor.preProcess(httpRequestMock, resourceMethodMock);

        verify(interceptorHelper).extractClientModuleFromHeader(requestCaptor.capture());
        String clienModuleHeader = requestCaptor.getValue().getHttpHeaders().getRequestHeader("odigeo-module-info").get(0);
        assertEquals(clienModuleHeader, "some-module:v1");
        assertEquals(IOUtils.toByteArray(httpRequestMock.getInputStream()), "".getBytes());
    }

    @Test
    public void testPreProcessMembershipDisableOperation() throws IOException, URISyntaxException {
        MembershipAutorenewalInterceptor membershipAutorenewalInterceptor = spy(new MembershipAutorenewalInterceptor());
        HttpRequest httpRequestMock = MockHttpRequest
                .put(MEMBERSHIP_DISABLE_URI)
                .header("odigeo-module-info", "some-module:v1");
        httpRequestMock.setInputStream(new ByteArrayInputStream("".getBytes()));
        InterceptorHelper interceptorHelper = spy(new InterceptorHelper());
        doReturn(interceptorHelper).when(membershipAutorenewalInterceptor).getInterceptorHelper();

        membershipAutorenewalInterceptor.preProcess(httpRequestMock, resourceMethodMock);

        verify(interceptorHelper).extractClientModuleFromHeader(requestCaptor.capture());
        String clienModuleHeader = requestCaptor.getValue().getHttpHeaders().getRequestHeader("odigeo-module-info").get(0);
        assertEquals(clienModuleHeader, "some-module:v1");
        assertEquals(IOUtils.toByteArray(httpRequestMock.getInputStream()), "".getBytes());
    }

}