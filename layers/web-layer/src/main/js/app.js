const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {bookingID: ''};
        this.state = {statusResponse: Boolean};
        this.state = {bookingSent: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    displayResults() {
    if (this.state.statusResponse) {
        alert('Booking ' + this.state.bookingSent + ' successfully sent to Membership Activation Retry Robot');
        return;
    }
        alert('Booking ' + this.state.bookingSent + ' cannot be processed for Membership activation retry');
    }

    handleChange (event) {
        this.setState({bookingID: event.target.value});
    }

    handleSubmit(event) {
        alert('A Booking was submitted: ' + this.state.bookingID);
        client({method: 'PUT', path: '/membership/membership/v1/retry-membership-activation/'+this.state.bookingID}).done(response => {
            this.setState({statusResponse :  response.entity});
            this.displayResults();
        });
        this.setState({bookingSent :  this.state.bookingID.toString()});
        this.setState({bookingID: ''});
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <ul>
                    <li>
                        <label>Booking ID:</label> <input type="text"  value={this.state.bookingID} onChange={this.handleChange} />
                    </li>
                    <li>
                        <input type="submit" value="Activate" />
                    </li>
                </ul>
            </form>
        );
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('react')
)
