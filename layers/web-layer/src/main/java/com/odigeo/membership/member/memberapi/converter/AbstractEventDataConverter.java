package com.odigeo.membership.member.memberapi.converter;

import com.google.gson.Gson;
import com.odigeo.tracking.client.track.method.EventDataConverter;

public abstract class AbstractEventDataConverter implements EventDataConverter {

    protected final Gson gson = new Gson();

    @Override
    public Object convertReturnToJson(Object returnValue) {
        return null;
    }

    @Override
    public Object convertExceptionJson(Throwable throwable) {
        return gson.toJson(new Object[]{throwable.getMessage(), throwable.getStackTrace()[0]});
    }
}
