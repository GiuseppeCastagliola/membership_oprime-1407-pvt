package com.odigeo.membership.member.memberapi.v1;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MembershipBackOfficeService;
import com.odigeo.membership.member.BackOfficeService;
import com.odigeo.membership.member.memberapi.util.LogUtils;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MembershipBackOfficeServiceController implements MembershipBackOfficeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipBackOfficeServiceController.class);

    @Override
    public Boolean updateMembershipMarketingInfo(Long membershipId, String email) {
        boolean result = false;
        try {
            result = getMembershipBackOfficeService().updateMembershipMarketingInfo(membershipId);
        } catch (MissingElementException e) {
            LogUtils.smallLog(LOGGER, "MissingElementException trying to send marketing message", e);
        } catch (DataAccessException e) {
            LogUtils.smallLog(LOGGER, "DataAccessException trying to send marketing message", e);
            throw MembershipExceptionMapper.map(e);
        }
        return result;
    }

    private BackOfficeService getMembershipBackOfficeService() {
        return ConfigurationEngine.getInstance(BackOfficeService.class);
    }
}
