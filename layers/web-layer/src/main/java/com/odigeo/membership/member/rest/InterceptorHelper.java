package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.util.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.resteasy.spi.HttpRequest;

import javax.ws.rs.Path;
import javax.ws.rs.core.PathSegment;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

@Singleton
public class InterceptorHelper {
    private static final String UNKNOWN_MODULE_NAME = "unknown module";
    private static final String MODULE_INFO_HEADER = "odigeo-module-info";
    private static final String PATH_SEGMENT_DIVIDER = "/";
    private static final String COOKIE_HEADER_DIVIDER = ":";
    private static final Logger LOGGER = Logger.getLogger(InterceptorHelper.class);

    private final StringUtils stringUtils = ConfigurationEngine.getInstance(StringUtils.class);

    String buildOneLineString(byte[] content) {
        return stringUtils.replaceNewLineOrTabByWhiteSpace(new String(content, StandardCharsets.UTF_8));
    }

    boolean isRequestOf(Method method, String... endpoints) {
        Path path = method.getAnnotation(Path.class);
        return Arrays.stream(endpoints)
                .anyMatch(endpoint -> Objects.nonNull(path) && path.value().contains(PATH_SEGMENT_DIVIDER + endpoint));
    }

    Optional<String> extractFieldFromBody(String body, Function<UpdateMembershipRequest, String> selector) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return Optional.ofNullable(mapper.readValue(body, UpdateMembershipRequest.class)).map(selector);
        } catch (IOException e) {
            LOGGER.warn("Missing field in request body: " + body);
        }
        return Optional.empty();
    }

    Optional<String> extractSegmentFromPath(HttpRequest request, int reverseIndex) {
        List<PathSegment> pathSegments = request.getUri().getPathSegments();
        if (pathSegments.size() > reverseIndex) {
            return Optional.ofNullable(pathSegments.get(pathSegments.size() - reverseIndex).toString());
        }
        return Optional.empty();
    }

    String extractClientModuleFromHeader(HttpRequest httpRequest) {
        return Optional.ofNullable(httpRequest.getHttpHeaders().getRequestHeader(MODULE_INFO_HEADER))
                .filter(requestHeaders -> !requestHeaders.isEmpty())
                .map(requestHeaders -> requestHeaders.get(0).split(COOKIE_HEADER_DIVIDER)[0])
                .filter(headerValue -> !headerValue.isEmpty())
                .orElse(UNKNOWN_MODULE_NAME);
    }

}
