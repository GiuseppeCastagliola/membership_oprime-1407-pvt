package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.UpdateMembershipAction;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.core.ResourceMethod;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.interception.AcceptedByMethod;
import org.jboss.resteasy.spi.interception.PreProcessInterceptor;

import javax.ws.rs.ext.Provider;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Optional;
import java.util.stream.Stream;

@Provider
@ServerInterceptor
public class MembershipAutorenewalInterceptor implements PreProcessInterceptor, AcceptedByMethod {

    private static final String UPDATE_MEMBERSHIP_PATH = "updateMembership";
    private static final String ENABLE_AUTO_RENEWAL_PATH = "enable-auto-renewal";
    private static final String DISABLE_AUTO_RENEWAL_PATH = "disable-auto-renewal";
    private static final String DISABLE_MEMBERSHIP_PATH = "disable";
    private static final String REACTIVATE_MEMBERSHIP_PATH = "reactivate";
    private static final String ACTIVATE_MEMBERSHIP_PATH = "activate";
    private static final String SUB_PATH = "/";
    private static final int AUTORENEWAL_REQUEST_ID_INDEX = 1;
    private static final int AUTORENEWAL_REQUEST_METHOD_INDEX = 2;
    private static final int UPDATE_MEMBERSHIP_REQUEST_METHOD_INDEX = 1;

    @Override
    public boolean accept(Class aClass, Method method) {
        return aClass.getSimpleName().equals("MembershipService")
                && getInterceptorHelper().isRequestOf(method, UPDATE_MEMBERSHIP_PATH, ENABLE_AUTO_RENEWAL_PATH,
                DISABLE_AUTO_RENEWAL_PATH, DISABLE_MEMBERSHIP_PATH + SUB_PATH, REACTIVATE_MEMBERSHIP_PATH + SUB_PATH,
                ACTIVATE_MEMBERSHIP_PATH + SUB_PATH);
    }

    @Override
    public ServerResponse preProcess(HttpRequest request, ResourceMethod resourceMethod) {
        extractFields(request).ifPresent(fields -> MetricsUtils.incrementCounter(
                MetricsBuilder.buildAutorenewalBySource(fields),
                MetricsNames.METRICS_REGISTRY_NAME)
        );
        return null;
    }

    private Optional<HashMap<String, String>> extractFields(HttpRequest request) {
        Optional<HashMap<String, String>> fields = Optional.empty();
        String path = request.getUri().getPath();
        if (path.contains(UPDATE_MEMBERSHIP_PATH)) {
            fields = extractInfoFromUpdateMembershipRequest(request);
        } else if (Stream.of(ENABLE_AUTO_RENEWAL_PATH, DISABLE_AUTO_RENEWAL_PATH, ACTIVATE_MEMBERSHIP_PATH, REACTIVATE_MEMBERSHIP_PATH, DISABLE_MEMBERSHIP_PATH).anyMatch(path::contains)) {
            fields = extractInfoFromAutorenewalRequest(request);
        }
        return fields;
    }

    private Optional<HashMap<String, String>> extractInfoFromAutorenewalRequest(HttpRequest request) {
        final HashMap<String, String> fields = new HashMap<>();
        return getInterceptorHelper().extractSegmentFromPath(request, AUTORENEWAL_REQUEST_ID_INDEX)
                .flatMap(membershipId -> {
                    fields.put(MetricsNames.AUTORENEWAL_ID, membershipId);
                    return getInterceptorHelper().extractSegmentFromPath(request, AUTORENEWAL_REQUEST_METHOD_INDEX);
                }).flatMap(method -> {
                    fields.put(MetricsNames.AUTORENEWAL_METHOD, method);
                    return toValidOperation(method);
                }).map(operation -> {
                    fields.put(MetricsNames.AUTORENEWAL_OPERATION, operation);
                    return getInterceptorHelper().extractClientModuleFromHeader(request);
                }).map(clientModule -> {
                    fields.put(MetricsNames.AUTORENEWAL_CLIENT_MODULE, clientModule);
                    return fields;
                });
    }

    private Optional<HashMap<String, String>> extractInfoFromUpdateMembershipRequest(HttpRequest request) {
        Optional<HashMap<String, String>> collectedFields = Optional.empty();
        byte[] content = new byte[0];
        try {
            content = IOUtils.toByteArray(request.getInputStream());
            String body = getInterceptorHelper().buildOneLineString(content);

            final HashMap<String, String> fields = new HashMap<>();
            collectedFields = getInterceptorHelper().extractFieldFromBody(body, UpdateMembershipRequest::getMembershipId)
                    .flatMap(membershipId -> {
                        fields.put(MetricsNames.AUTORENEWAL_ID, membershipId);
                        return getInterceptorHelper().extractSegmentFromPath(request, UPDATE_MEMBERSHIP_REQUEST_METHOD_INDEX);
                    })
                    .flatMap(method -> {
                        fields.put(MetricsNames.AUTORENEWAL_METHOD, method);
                        return getInterceptorHelper().extractFieldFromBody(body, UpdateMembershipRequest::getOperation).flatMap(this::toValidOperation);
                    })
                    .map(operation -> {
                        fields.put(MetricsNames.AUTORENEWAL_OPERATION, operation);
                        return getInterceptorHelper().extractClientModuleFromHeader(request);
                    }).map(clientModule -> {
                        fields.put(MetricsNames.AUTORENEWAL_CLIENT_MODULE, clientModule);
                        return fields;
                    });

        } catch (IOException e) {
            return collectedFields;
        } finally {
            request.setInputStream(new ByteArrayInputStream(content));
        }
        return collectedFields;
    }

    private Optional<String> toValidOperation(String value) {
        Optional<String> operation = Optional.empty();
        if (value.contains(UpdateMembershipAction.ENABLE_AUTO_RENEWAL.name())
                || value.contains(ENABLE_AUTO_RENEWAL_PATH)
                || value.contains(ACTIVATE_MEMBERSHIP_PATH)
                || value.contains(REACTIVATE_MEMBERSHIP_PATH)) {
            operation = Optional.of(UpdateMembershipAction.ENABLE_AUTO_RENEWAL.name());
        } else if (value.contains(UpdateMembershipAction.DISABLE_AUTO_RENEWAL.name())
                || value.contains(DISABLE_AUTO_RENEWAL_PATH)
                || value.contains(DISABLE_MEMBERSHIP_PATH)) {
            operation = Optional.of(UpdateMembershipAction.DISABLE_AUTO_RENEWAL.name());
        }
        return operation;
    }

    InterceptorHelper getInterceptorHelper() {
        return ConfigurationEngine.getInstance(InterceptorHelper.class);
    }
}
