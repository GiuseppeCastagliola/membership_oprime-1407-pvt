package com.odigeo.membership.robots.retry;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.retry.MembershipActivationRetryService;
import com.odigeo.messaging.kafka.MessageIntegrationService;
import com.odigeo.messaging.message.MembershipActivationRetryMessage;
import com.odigeo.messaging.utils.Consumer;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.robots.RobotExecutionResult;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class MembershipActivationRetryRobotTest {

    @Mock
    private MessageIntegrationService<MembershipActivationRetryMessage> messageIntegrationService;
    @Mock
    private MembershipActivationRetryService membershipActivationRetryService;
    @Mock
    private Consumer<MembershipActivationRetryMessage> kafkaJsonConsumer;
    @Mock
    private ConsumerIterator<MembershipActivationRetryMessage> consumerMessageIterator;
    @Mock
    private MembershipActivationRetryExecutors executors;
    @Mock
    private ExecutorService executorService;

    private MembershipActivationRetryRobot membershipActivationRetryRobot;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        doReturn(kafkaJsonConsumer).when(messageIntegrationService)
                .createKafkaConsumer(anyString(), anyString(), anyString(), any());
        doReturn(Collections.singletonList(consumerMessageIterator))
                .when(kafkaJsonConsumer).connectAndIterate(anyInt());
        doReturn(executorService).when(executors).getConsumerPool();
        doReturn(kafkaJsonConsumer).when(executors).getConsumer();
        membershipActivationRetryRobot = new MembershipActivationRetryRobot("TestRobot");
    }

    private void configure(Binder binder) {
        binder.bind(MembershipActivationRetryExecutors.class).toInstance(executors);
        binder.bind(MessageIntegrationService.class).toInstance(messageIntegrationService);
        binder.bind(MembershipActivationRetryService.class).toInstance(membershipActivationRetryService);
    }

    @Test
    public void testExecuteHappyPath() {
        assertEquals(membershipActivationRetryRobot.execute(new HashMap<>()), RobotExecutionResult.OK);
        verify(executorService, times(1)).execute(any(MembershipActivationRetryWorker.class));
    }

    @Test
    public void testInterruptHappyPath() throws InterruptedException {
        membershipActivationRetryRobot.interrupt();
        verify(executors, times(1)).stop();
    }

    @Test
    public void testInterruptWhenInterruptedException() {
        try {
            doThrow(new InterruptedException()).when(executorService).awaitTermination(anyLong(), any());
            membershipActivationRetryRobot.interrupt();
        } catch (InterruptedException e) {
            fail();
        }
        verify(executors, times(1)).stop();
    }
}
