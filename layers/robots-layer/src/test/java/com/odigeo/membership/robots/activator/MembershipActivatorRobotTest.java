package com.odigeo.membership.robots.activator;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.robots.BookingUpdatesReaderService;
import com.odigeo.robots.RobotExecutionResult;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class MembershipActivatorRobotTest {

    private static final String TEST_ROBOT = "TestRobot";

    @Mock
    private MembershipActivatorRobot membershipActivatorRobot;
    @Mock
    private BookingUpdatesReaderService bookingUpdatesReaderService;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        membershipActivatorRobot = new MembershipActivatorRobot(TEST_ROBOT);
    }

    private void configure(Binder binder) {
        binder.bind(BookingUpdatesReaderService.class).toInstance(bookingUpdatesReaderService);
    }

    @Test
    public void testConstructor() {
        assertNotNull(new MembershipActivatorRobot(TEST_ROBOT));
    }

    @Test
    public void testExecuteCritical() {
        assertEquals(membershipActivatorRobot.execute(new HashMap<>()), RobotExecutionResult.CRITICAL);
    }

    @Test
    public void testExecuteOK() {
        when(bookingUpdatesReaderService.startActivationConsumerController()).thenReturn(true);
        assertEquals(membershipActivatorRobot.execute(new HashMap<>()), RobotExecutionResult.OK);
    }

    @Test
    public void testInterrupt() {
        membershipActivatorRobot.execute(new HashMap<>());
        membershipActivatorRobot.interrupt();
        verify(bookingUpdatesReaderService, times(1)).stopActivationConsumerController();
    }
}
