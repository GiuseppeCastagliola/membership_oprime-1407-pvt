package com.odigeo.membership.robots.retry;

import com.google.inject.Inject;
import com.odigeo.messaging.MembershipActivationRetryMessageConfigurationManager;
import com.odigeo.messaging.kafka.MessageIntegrationService;
import com.odigeo.messaging.message.MembershipActivationRetryMessage;
import com.odigeo.messaging.utils.Consumer;
import org.apache.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class MembershipActivationRetryExecutors {

    private final ScheduledExecutorService scheduledPool;
    private final ExecutorService consumerPool;
    private final Consumer<MembershipActivationRetryMessage> consumer;
    private final MembershipActivationRetryMessageConfigurationManager manager;
    private static final Logger logger = Logger.getLogger(MembershipActivationRetryExecutors.class);

    @Inject
    MembershipActivationRetryExecutors(MembershipActivationRetryMessageConfigurationManager manager, MessageIntegrationService messageIntegrationService) {
        this.manager = manager;
        scheduledPool = Executors.newScheduledThreadPool(manager.getThreadsPerIterator());
        consumerPool = Executors.newFixedThreadPool(manager.getNumberOfIterators());
        consumer = messageIntegrationService
                .createKafkaConsumer(manager.getConsumerList(),
                        manager.getConsumerGroup(),
                        manager.getTopicName(),
                        MembershipActivationRetryMessage.class);
    }

    ScheduledExecutorService getScheduledPool() {
        return scheduledPool;
    }

    ExecutorService getConsumerPool() {
        return consumerPool;
    }

    Consumer<MembershipActivationRetryMessage> getConsumer() {
        return consumer;
    }

    public void stop() {
        logger.error("MembershipActivationRetryExecutors STOPPED");
        getScheduledPool().shutdown();
        getConsumerPool().shutdown();
        try {
            getScheduledPool().awaitTermination(manager.getRetryPeriod(), TimeUnit.valueOf(manager.getRetryPeriodUnit()));
            getConsumerPool().awaitTermination(manager.getRetryPeriod(), TimeUnit.valueOf(manager.getRetryPeriodUnit()));
        } catch (InterruptedException e) {
            logger.error("InterruptedException when shutting down executor service " + e.getMessage(), e);
        }
        getConsumer().close();
    }
}
