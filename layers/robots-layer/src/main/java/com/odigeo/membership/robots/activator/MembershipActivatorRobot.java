package com.odigeo.membership.robots.activator;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.robots.BookingUpdatesReaderService;
import com.odigeo.robots.AbstractRobot;
import com.odigeo.robots.RobotExecutionResult;
import org.apache.log4j.Logger;

import java.util.Map;

public class MembershipActivatorRobot extends AbstractRobot {

    private static final Logger LOGGER = Logger.getLogger(MembershipActivatorRobot.class);

    @Inject
    public MembershipActivatorRobot(@Assisted String robotId) {
        super(robotId);
    }

    @Override
    public RobotExecutionResult execute(Map<String, String> configuration) {
        final boolean success = getBookingUpdatesService().startActivationConsumerController();
        LOGGER.info("Reader finished successfully: " + success);
        return success ? RobotExecutionResult.OK : RobotExecutionResult.CRITICAL;
    }

    @Override
    public void interrupt() {
        getBookingUpdatesService().stopActivationConsumerController();
        super.interrupt();
    }

    private BookingUpdatesReaderService getBookingUpdatesService() {
        return ConfigurationEngine.getInstance(BookingUpdatesReaderService.class);
    }
}
