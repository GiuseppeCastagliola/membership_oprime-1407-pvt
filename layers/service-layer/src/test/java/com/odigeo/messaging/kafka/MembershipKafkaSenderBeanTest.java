package com.odigeo.messaging.kafka;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.propertiesconfig.PropertiesCacheService;
import com.odigeo.membership.robots.activator.MembershipActivationProducerConfiguration;
import com.odigeo.membership.robots.reporter.MembershipUpdateProducerConfiguration;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.Publisher;
import com.odigeo.visitengineapi.last.multitest.TestAssignmentService;
import com.odigeo.visitengineapi.last.multitest.TestDimensionPartitionService;
import org.apache.commons.lang.StringUtils;
import org.apache.kafka.clients.producer.Callback;
import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.verification.VerificationMode;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.Random;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class MembershipKafkaSenderBeanTest {

    private static final String EMAIL = "test@gmail.com";
    private static final String LOCALHOST = "localhost:9092";
    private static final VerificationMode TWO_TIMES = times(2);

    @Mock
    private MembershipActivationProducerConfiguration membershipActivationProducerConfiguration;
    @Mock
    private MembershipUpdateProducerConfiguration membershipUpdateProducerConfiguration;
    @Mock
    private MembershipSubscriptionMessage membershipSubscriptionMessage;
    @Mock
    private MembershipUpdateMessage membershipUpdateMessage;
    @Mock
    private MessageIntegrationServiceBean<MembershipSubscriptionMessage> subscriptionMessageIntegrationServiceBean;
    @Mock
    private MessageIntegrationServiceBean<MembershipUpdateMessage> updateMessageIntegrationServiceBean;
    @Mock
    private Publisher<MembershipSubscriptionMessage> membershipSubscriptionMessagePublisher;
    @Mock
    private Publisher<MembershipUpdateMessage> membershipUpdateMessagePublisher;
    @Mock
    private Appender mockAppender;
    @Mock
    private TestDimensionPartitionService TestDimensionPartitionServiceMock;
    @Mock
    private TestAssignmentService testAssignmentServiceMock;
    @Mock
    private PropertiesCacheService propertiesCacheServiceMock;
    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

    private MembershipKafkaSenderBean membershipSenderMessageServiceBean;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        Logger.getRootLogger().addAppender(mockAppender);
        when(membershipActivationProducerConfiguration.getBrokerList()).thenReturn(LOCALHOST);
        when(membershipUpdateProducerConfiguration.getBrokerList()).thenReturn(LOCALHOST);
        when(subscriptionMessageIntegrationServiceBean.createKafkaPublisher(eq(membershipActivationProducerConfiguration.getBrokerList()))).thenReturn(membershipSubscriptionMessagePublisher);
        when(updateMessageIntegrationServiceBean.createKafkaPublisher(eq(membershipUpdateProducerConfiguration.getBrokerList()))).thenReturn(membershipUpdateMessagePublisher);
        membershipSenderMessageServiceBean = new MembershipKafkaSenderBean(membershipActivationProducerConfiguration, membershipUpdateProducerConfiguration, subscriptionMessageIntegrationServiceBean, updateMessageIntegrationServiceBean, propertiesCacheServiceMock);
        ConfigurationEngine.init(binder -> binder.bind(TestDimensionPartitionService.class).toInstance(TestDimensionPartitionServiceMock),
                binder -> binder.bind(TestAssignmentService.class).toInstance(testAssignmentServiceMock));
    }

    @AfterMethod
    public void tearDown() {
        Logger.getRootLogger().removeAppender(mockAppender);
    }

    @Test
    public void testSendMembershipSubscriptionMessageToKafka() throws MessageDataAccessException {
        membershipSenderMessageServiceBean.sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessage);
        membershipSenderMessageServiceBean.sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessage);
        verify(subscriptionMessageIntegrationServiceBean).createKafkaPublisher(eq(membershipActivationProducerConfiguration.getBrokerList()));
        verify(membershipSubscriptionMessagePublisher, TWO_TIMES).publishMessage(any(), any());
    }

    @Test
    public void testSendMembershipUpdateMessageToKafkaNotSentWhenNotActive() throws MessageDataAccessException {
        when(propertiesCacheServiceMock.isSendingIdsToKafkaActive()).thenReturn(false);
        membershipSenderMessageServiceBean.sendMembershipUpdateMessageToKafka(membershipUpdateMessage);
        verify(subscriptionMessageIntegrationServiceBean).createKafkaPublisher(eq(membershipUpdateProducerConfiguration.getBrokerList()));
        verifyZeroInteractions(membershipUpdateMessagePublisher);
    }

    @Test
    public void testSendMembershipUpdateMessageToKafkaSentWhenActive() throws MessageDataAccessException {
        when(propertiesCacheServiceMock.isSendingIdsToKafkaActive()).thenReturn(true);
        membershipSenderMessageServiceBean.sendMembershipUpdateMessageToKafka(membershipUpdateMessage);
        verify(subscriptionMessageIntegrationServiceBean).createKafkaPublisher(eq(membershipUpdateProducerConfiguration.getBrokerList()));
        verify(membershipUpdateMessagePublisher).publishMessage(eq(membershipUpdateMessage), any());
    }

    @Test(expectedExceptions = MessageDataAccessException.class)
    public void testSendMembershipSubscriptionMessageToKafkaExceptionally() throws MessageDataAccessException {
        doThrow(new MessageDataAccessException(StringUtils.EMPTY, new Exception())).when(membershipSubscriptionMessagePublisher).publishMessage(eq(membershipSubscriptionMessage), any(Callback.class));
        membershipSenderMessageServiceBean.sendMembershipSubscriptionMessageToKafka(membershipSubscriptionMessage);
    }

    @Test
    public void testPublishMessageCallbackProcessorNonNullException() {
        String exceptionMessage = "impossible to send message";
        Exception e = new Exception(exceptionMessage);
        membershipSenderMessageServiceBean.publishMessageCallbackProcessor(MembershipKafkaSenderBean.SUBSCRIPTION_MESSAGE_SENT_OK, EMAIL, MetricsNames.FAILED_SUBSCRIPTION_MSG, e);
        verify(mockAppender).doAppend(captorLoggingEvent.capture());
        LoggingEvent loggingEvent = captorLoggingEvent.getAllValues().get(0);
        assertEquals(exceptionMessage, loggingEvent.getMessage());
        assertEquals(Level.ERROR, loggingEvent.getLevel());
    }

    @Test
    public void testPublishMessageCallbackProcessorNullException() {
        membershipSenderMessageServiceBean.publishMessageCallbackProcessor(MembershipKafkaSenderBean.SUBSCRIPTION_MESSAGE_SENT_OK, EMAIL, MetricsNames.SUCCESS_SUBSCRIPTION_MSG, null);
        verify(mockAppender).doAppend(captorLoggingEvent.capture());
        LoggingEvent loggingEvent = captorLoggingEvent.getAllValues().get(0);
        assertEquals(loggingEvent.getMessage(), MembershipKafkaSenderBean.SUBSCRIPTION_MESSAGE_SENT_OK.replace("{}", EMAIL));
        assertEquals(Level.INFO, loggingEvent.getLevel());
    }

    @Test
    public void testGetMembershipSubscriptionMessage() throws BuilderException {
        BookingDetail bookingDetail = new BookingDetailBuilder().build(new Random());
        LocalDateTime activationDate = LocalDateTime.now();
        LocalDateTime expirationDate = activationDate.plusMonths(6);
        Membership membership = new MembershipBuilder()
                .setActivationDate(activationDate)
                .setExpirationDate(expirationDate).build();
        MembershipSubscriptionMessage membershipSubscriptionMessage = MembershipKafkaSenderBean
                .getMembershipSubscriptionMessage(bookingDetail, membership, SubscriptionStatus.SUBSCRIBED);
        assertEquals(membershipSubscriptionMessage.getEmail(), bookingDetail.getBuyer().getMail());
        assertEquals(membershipSubscriptionMessage.getKey(), bookingDetail.getBuyer().getMail());
        assertEquals(membershipSubscriptionMessage.getWebsite(), bookingDetail.getBookingBasicInfo().getWebsite().getCode());
        assertEquals(membershipSubscriptionMessage.getActivationDate(), activationDate.toLocalDate());
        assertEquals(membershipSubscriptionMessage.getExpirationDate(), expirationDate.toLocalDate());
        assertEquals(membershipSubscriptionMessage.getSubscriptionStatus(), SubscriptionStatus.SUBSCRIBED);
    }
}
