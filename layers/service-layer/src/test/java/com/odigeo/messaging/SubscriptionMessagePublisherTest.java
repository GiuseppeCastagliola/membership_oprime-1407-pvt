package com.odigeo.messaging;

import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.userapi.UserInfo;
import com.odigeo.userprofiles.api.v1.model.UserBasicInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class SubscriptionMessagePublisherTest {

    private static final Long USER_ID = 343L;
    private static final String EMAIL = "user@odigeo.com";
    private static final Membership activeMembership = new MembershipBuilder()
            .setId(123L).setStatus(MemberStatus.ACTIVATED).build();

    @Mock
    private Appender mockAppender;
    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;
    @Mock
    private MembershipMessageSendingService membershipMessageSendingServiceMock;
    @Mock
    private MembershipSubscriptionMessageService membershipSubscriptionMessageServiceMock;

    private SubscriptionMessagePublisher subscriptionMessagePublisher;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        Logger.getRootLogger().addAppender(mockAppender);
        subscriptionMessagePublisher = new SubscriptionMessagePublisher(membershipMessageSendingServiceMock, membershipSubscriptionMessageServiceMock);
    }

    @AfterMethod
    public void tearDown() {
        Logger.getRootLogger().removeAppender(mockAppender);
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopic_HappyPath() throws DataNotFoundException {
        MembershipSubscriptionMessage subscriptionMessage = mock(MembershipSubscriptionMessage.class);
        when(membershipSubscriptionMessageServiceMock.getMembershipSubscriptionMessage(activeMembership, SubscriptionStatus.SUBSCRIBED))
                .thenReturn(subscriptionMessage);
        subscriptionMessagePublisher.sendSubscriptionMessageToCRMTopic(activeMembership, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingServiceMock).sendCompletedMembershipSubscriptionMessage(subscriptionMessage);
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopic_ExceptionRetrievingUserInfo() throws DataNotFoundException {
        doThrow(new DataNotFoundException(StringUtils.EMPTY))
                .when(membershipSubscriptionMessageServiceMock).getMembershipSubscriptionMessage(activeMembership, SubscriptionStatus.SUBSCRIBED);
        subscriptionMessagePublisher.sendSubscriptionMessageToCRMTopic(activeMembership, SubscriptionStatus.SUBSCRIBED);
        verifyLogEventsWhenExceptionOcurred();
    }


    @Test
    public void testSendSubscriptionMessageToCRMTopicWithUserInfo_HappyPath() throws DataNotFoundException {
        final UserInfo userInfo = getUserInfo();
        MembershipSubscriptionMessage subscriptionMessage = mock(MembershipSubscriptionMessage.class);
        when(membershipSubscriptionMessageServiceMock.buildMembershipSubscriptionMessage(userInfo, activeMembership, SubscriptionStatus.SUBSCRIBED))
                .thenReturn(subscriptionMessage);
        subscriptionMessagePublisher.sendSubscriptionMessageToCRMTopic(userInfo, activeMembership, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingServiceMock).sendCompletedMembershipSubscriptionMessage(subscriptionMessage);
    }

    @Test
    public void testSendSubscriptionMessageToCRMTopicWithUserInfo_ExceptionRetrievingUserInfo() throws DataNotFoundException {
        final UserInfo userInfo = getUserInfo();
        doThrow(new DataNotFoundException(StringUtils.EMPTY))
                .when(membershipSubscriptionMessageServiceMock).buildMembershipSubscriptionMessage(userInfo, activeMembership, SubscriptionStatus.SUBSCRIBED);
        subscriptionMessagePublisher.sendSubscriptionMessageToCRMTopic(userInfo, activeMembership, SubscriptionStatus.SUBSCRIBED);
        verifyLogEventsWhenExceptionOcurred();
    }

    private void verifyLogEventsWhenExceptionOcurred() {
        verify(mockAppender, times(2)).doAppend(captorLoggingEvent.capture());
        List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();
        assertEquals(loggingEvents.size(), 2);
        assertEquals(Level.INFO, loggingEvents.get(0).getLevel());
        assertEquals(Level.WARN, loggingEvents.get(1).getLevel());
    }

    private UserInfo getUserInfo() {
        final UserBasicInfo userBasicInfo = new UserBasicInfo();
        userBasicInfo.setEmail(EMAIL);
        userBasicInfo.setId(USER_ID);
        return UserInfo.fromUserBasicInfo(userBasicInfo);
    }
}