package com.odigeo.messaging;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.messaging.kafka.MessageIntegrationService;
import com.odigeo.messaging.kafka.MessageIntegrationServiceBean;
import com.odigeo.messaging.message.MembershipActivationRetryMessage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class MessageIntegrationServiceBeanTest {

    private MessageIntegrationService messageIntegrationService;
    private MembershipActivationRetryMessageConfigurationManager configurationManager;

    @BeforeMethod
    public void setUp() {
        ConfigurationEngine.init();
        configurationManager = ConfigurationEngine.getInstance(MembershipActivationRetryMessageConfigurationManager.class);
        messageIntegrationService = new MessageIntegrationServiceBean();
    }

    @Test
    public void testCreateKafkaPublisher() {
        assertNotNull(messageIntegrationService.createKafkaPublisher(configurationManager.getBrokerProducerList()));
    }

    @Test
    public void testCreateKafkaConsumer() {
        assertNotNull(messageIntegrationService.createKafkaConsumer(configurationManager.getConsumerList(),
                configurationManager.getConsumerGroup(),
                configurationManager.getTopicName(),
                MembershipActivationRetryMessage.class));
    }
}
