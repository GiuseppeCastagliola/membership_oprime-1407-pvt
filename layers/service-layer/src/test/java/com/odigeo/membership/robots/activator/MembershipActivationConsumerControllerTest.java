package com.odigeo.membership.robots.activator;

import com.odigeo.membership.retry.MembershipActivationRetryService;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.Consumer;
import com.odigeo.messaging.utils.ConsumerIterator;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.verification.VerificationMode;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Test
public class MembershipActivationConsumerControllerTest {

    private static final VerificationMode ONE_TIME = Mockito.times(1);
    private static final int NUM_THREADS = 2;

    @Mock
    private Consumer<BasicMessage> consumer;
    @Mock
    private BookingUpdatesConsumerConfiguration bookingUpdatesConsumerConfiguration;
    @Mock
    private ExecutorService executorService;
    @Mock
    private Future mockFuture, mockFutureA, mockFutureB;
    @Mock
    private MembershipActivationService membershipActivationService;
    @Mock
    private MembershipActivationRetryService membershipActivationRetryService;
    @Mock
    private BookingTrackingService bookingTrackingService;

    private MembershipActivationConsumerController membershipActivationConsumerController;


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        setConfigurationValues();
        ConsumerIterator<BasicMessage> basicMessageConsumerIterator = configureMockConsumerIterator(mockFuture, "1", "2", "3");
        when(consumer.connectAndIterate(eq(1))).thenReturn(Collections.singletonList(basicMessageConsumerIterator));
        when(consumer.connectAndIterate(eq(NUM_THREADS))).thenReturn(Arrays.asList(basicMessageConsumerIterator, basicMessageConsumerIterator));
        membershipActivationConsumerController = new MembershipActivationConsumerController(consumer, executorService);
    }

    public void testStartMultiThreadConsumer() {
        membershipActivationConsumerController.startMultiThreadConsumer();
        verify(executorService, Mockito.times(NUM_THREADS)).submit(any(MembershipActivationConsumerWorker.class));
        verify(consumer, ONE_TIME).connectAndIterate(eq(NUM_THREADS));
    }

    public void testStartMultipleMultiThreadConsumer() throws Exception {
        String[] keysA = {"10", "20", "30"};
        ConsumerIterator<BasicMessage> iteratorA = configureMockConsumerIterator(mockFutureA, keysA);
        String[] keysB = {"40", "50", "60"};
        ConsumerIterator<BasicMessage> iteratorB = configureMockConsumerIterator(mockFutureB, keysB);
        when(mockFutureA.get()).thenReturn("Something happens");
        List<ConsumerIterator<BasicMessage>> consumerIterators = Arrays.asList(iteratorA, iteratorB);
        when(consumer.connectAndIterate(eq(NUM_THREADS))).thenReturn(consumerIterators);
        membershipActivationConsumerController.startMultiThreadConsumer();
        verify(executorService, Mockito.times(NUM_THREADS)).submit(any(MembershipActivationConsumerWorker.class));
        verify(mockFutureA, ONE_TIME).get();
        verify(mockFutureB, ONE_TIME).get();
    }

    public void testExceptionallyResultMultiThreadConsumer() throws Exception {
        when(mockFuture.get()).thenThrow(new InterruptedException());
        membershipActivationConsumerController.startMultiThreadConsumer();
        verify(mockFuture, Mockito.times(NUM_THREADS)).get();
    }

    public void testStopMultiThreadConsumer() {
        membershipActivationConsumerController.stopMultiThreadConsumer();
        verify(executorService, ONE_TIME).shutdown();
        verify(consumer, ONE_TIME).close();
    }

    public void testJoin() throws Exception {
        membershipActivationConsumerController.join();
        verify(executorService, ONE_TIME).awaitTermination(eq(Long.MAX_VALUE), eq(TimeUnit.DAYS));
    }

    @Test(expectedExceptions = Exception.class)
    public void testRealConfigurationTimeout() {
        membershipActivationConsumerController = new MembershipActivationConsumerController(bookingUpdatesConsumerConfiguration,
                membershipActivationService,
                membershipActivationRetryService,
                bookingTrackingService);
        membershipActivationConsumerController.startMultiThreadConsumer();
    }

    private ConsumerIterator<BasicMessage> configureMockConsumerIterator(Future future, String... keys) {
        ConsumerIterator<BasicMessage> messageConsumerIterator = new MockBasicMessageConsumerIterator(keys);
        when(executorService
                .submit(eq(new MembershipActivationConsumerWorker(messageConsumerIterator, membershipActivationService, membershipActivationRetryService, bookingTrackingService))))
                .thenReturn(future);
        return messageConsumerIterator;
    }

    private void setConfigurationValues() {
        when(bookingUpdatesConsumerConfiguration.getZooKeeperUrl()).thenReturn("4.4.4.4");
        when(bookingUpdatesConsumerConfiguration.getNumConsumerThreads()).thenReturn(NUM_THREADS);
        when(bookingUpdatesConsumerConfiguration.getActivatorConsumerGroup()).thenReturn("MembershipActivatorRobot");
        when(bookingUpdatesConsumerConfiguration.getTopicName()).thenReturn("BOOKING_UPDATES_v1");
    }
}
