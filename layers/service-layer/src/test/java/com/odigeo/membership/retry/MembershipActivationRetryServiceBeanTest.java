package com.odigeo.membership.retry;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.robots.activator.MembershipActivationService;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.messaging.MembershipActivationRetryMessageConfigurationManager;
import com.odigeo.messaging.kafka.MessageIntegrationService;
import com.odigeo.messaging.message.MembershipActivationRetryMessage;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.Publisher;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;


public class MembershipActivationRetryServiceBeanTest {

    private static final long ASYNC_FUTURE_EXECUTION_TIMEOUT_MS = 100;

    @Mock
    private MessageIntegrationService<MembershipActivationRetryMessage> messageIntegrationService;
    @Mock
    private Publisher<MembershipActivationRetryMessage> publisher;
    @Mock
    private MembershipActivationService membershipActivationService;
    @Mock
    private MembershipActivationRetryMessageConfigurationManager messageConfigurationManager;
    @Mock
    private BookingTrackingService bookingTrackingService;

    private MembershipActivationRetryService membershipActivationRetryService;
    private static final long BOOKING_ID = 123456L;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        ConfigurationEngine.init(this::configure);
        membershipActivationRetryService = new MembershipActivationRetryServiceBean();
        Mockito.doReturn(publisher)
                .when(messageIntegrationService)
                .createKafkaPublisher(Matchers.anyString());
        Mockito.doReturn(true)
                .when(membershipActivationService)
                .processBookingId(Matchers.anyLong());
        Mockito.doReturn("SECONDS").when(messageConfigurationManager).getRetryPeriodUnit();
        Mockito.doReturn(3).when(messageConfigurationManager).getRetryAttempts();
    }


    @Test
    public void testRetryHappyPath() {
        assertTrue(membershipActivationRetryService.retryActivation(MembershipActivationRetryMessage.createMessage(BOOKING_ID)));
    }

    @Test
    public void testRetryHappyPathNextAttempt() {
        assertTrue(membershipActivationRetryService.retryActivation(
                MembershipActivationRetryMessage.createMessageForNextAttempt(
                        MembershipActivationRetryMessage.createMessage(123456L))));
    }

    @Test
    public void testTrackingCalledIfBookingProcessed() throws Exception {
        membershipActivationRetryService.retryActivation(MembershipActivationRetryMessage.createMessage(BOOKING_ID));
        MILLISECONDS.sleep(ASYNC_FUTURE_EXECUTION_TIMEOUT_MS);
        verify(bookingTrackingService).processBooking(BOOKING_ID);
    }

    @Test
    public void retryActivationSuccessfulIfTrackingThrowsException() throws Exception {
        doThrow(new RetryableException("Something wrong with tracking")).when(bookingTrackingService).processBooking(BOOKING_ID);
        assertTrue(membershipActivationRetryService.retryActivation(MembershipActivationRetryMessage.createMessage(BOOKING_ID)));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testRetryWhenBookingIdIsNull() {
        assertFalse(membershipActivationRetryService.retryActivation(MembershipActivationRetryMessage.createMessage(null)));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testRetryWhenCreatingNullMessage() {
        assertFalse(membershipActivationRetryService.retryActivation(MembershipActivationRetryMessage.createMessageForNextAttempt(null)));
    }

    @Test
    public void testRetryWhenActivationFailsWithRemainingAttempts() throws MessageDataAccessException{
        Mockito.doReturn(false)
                .when(membershipActivationService)
                .processBookingId(Matchers.anyLong());
        assertTrue(membershipActivationRetryService.retryActivation(MembershipActivationRetryMessage.createMessage(BOOKING_ID)));
        verify(publisher, Mockito.times(1)).publishMessages(anyListOf(MembershipActivationRetryMessage.class));
    }

    @Test
    public void testRetryWhenMessageDataAccessException() throws MessageDataAccessException{
        doThrow(new MessageDataAccessException("MessageDataAccessException", new Throwable("MessageDataAccessException")))
                .when(publisher).publishMessages(anyListOf(MembershipActivationRetryMessage.class));
        Mockito.doReturn(false)
                .when(membershipActivationService)
                .processBookingId(Matchers.anyLong());

        assertFalse(membershipActivationRetryService.retryActivation(MembershipActivationRetryMessage.createMessage(BOOKING_ID)));
        verify(publisher).publishMessages(anyListOf(MembershipActivationRetryMessage.class));
    }

    @Test
    public void testRetryWhenMaxRetryAttemptsReached() {
        Mockito.doReturn(false)
                .when(membershipActivationService)
                .processBookingId(Matchers.anyLong());
        assertFalse(
                membershipActivationRetryService.retryActivation(
                        getMessageWithNoMoreAttempts(MembershipActivationRetryMessage.createMessage(BOOKING_ID))));
    }

    @Test
    public void testRetryWhenMessageIsNull() {
        assertFalse(membershipActivationRetryService.retryActivation(null));
    }

    @Test
    public void testScheduleForRetryHappyPath() {
        assertTrue(membershipActivationRetryService.scheduleForRetry(12345678L));
    }

    @Test
    public void testScheduleForRetryWhenBookingIdIsNull() {
        assertFalse(membershipActivationRetryService.scheduleForRetry(null));
    }

    @Test
    public void testScheduleForRetryWhenMessageDataAccessException() throws MessageDataAccessException {
        doThrow(new MessageDataAccessException("MessageDataAccessException", new Throwable("MessageDataAccessException")))
                .when(publisher)
                .publishMessages(anyListOf(MembershipActivationRetryMessage.class));
        assertFalse(membershipActivationRetryService.scheduleForRetry(BOOKING_ID));
    }

    private void configure(Binder t) {
        t.bind(MembershipActivationRetryMessageConfigurationManager.class).toInstance(messageConfigurationManager);
        t.bind(MessageIntegrationService.class).toInstance(messageIntegrationService);
        t.bind(MembershipActivationService.class).toInstance(membershipActivationService);
        t.bind(BookingTrackingService.class).toInstance(bookingTrackingService);
    }

    private MembershipActivationRetryMessage getMessageWithNoMoreAttempts(MembershipActivationRetryMessage message) {
        if (message.getRetryAttempts() == 0) {
            return message;
        }
        return getMessageWithNoMoreAttempts(MembershipActivationRetryMessage.createMessageForNextAttempt(message));
    }

}
