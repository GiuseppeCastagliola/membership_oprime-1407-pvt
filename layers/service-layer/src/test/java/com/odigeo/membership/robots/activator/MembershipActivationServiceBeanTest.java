package com.odigeo.membership.robots.activator;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.bookingapi.BookingApiManager;
import com.odigeo.bookingapi.v12.responses.BookingBasicInfo;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.Buyer;
import com.odigeo.bookingapi.v12.responses.InsuranceBooking;
import com.odigeo.bookingapi.v12.responses.MembershipSubscriptionBooking;
import com.odigeo.bookingapi.v12.responses.ProductCategoryBooking;
import com.odigeo.bookingapi.v12.responses.UserAgentInfo;
import com.odigeo.bookingapi.v12.responses.Website;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.activation.validation.util.ShoppingCartCollectionState;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.MemberAccountService;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.messaging.utils.MessageDataAccessException;
import org.apache.commons.collections.CollectionUtils;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

public class MembershipActivationServiceBeanTest {

    private static final String TEST_BOOKING_STATUS = "CONTRACT";
    private static final long TEST_MEMBERSHIP_ID = 1L;
    private static final long TEST_USER_ID = 1L;
    private static final long TEST_MEMBER_ACCOUNT_ID = 11L;
    private static final String TEST_WEBSITE = "ES";
    private static final String TEST_EMAIL = "test@edreams.com";
    private static final MemberStatus TEST_STATUS = MemberStatus.PENDING_TO_ACTIVATE;
    private static final Long TEST_BOOKING_ID = 182L;
    private static final String PRODUCT_MEMBERSHIP = "MEMBERSHIP_SUBSCRIPTION";
    private static final int TEST_DEFAULT_DURATION = 12;
    private static final MembershipType MEMBERSHIP_TYPE = MembershipType.BASIC;
    private static final String NAME = "Dan";
    private static final String LASTNAME = "Smith";

    @Mock
    private BookingApiManager bookingApiManagerMock;
    @Mock
    private MembershipActivationServiceBean membershipActivationServiceBeanMock;
    @Mock
    private MemberService memberServiceMock;
    @Mock
    private MemberAccountService memberAccountServiceMock;
    @Mock
    private BookingTrackingService bookingTrackingServiceMock;
    @Mock
    private BookingDetail bookingDetailMock;
    @Mock
    private Website websiteMock;
    @Mock
    private Buyer buyer;
    @Mock
    private MembershipActivationProducerConfiguration membershipActivationProducerConfigurationMock;
    @Mock
    private MembershipSubscriptionBooking membershipSubscriptionBookingMock;
    @Mock
    private BookingBasicInfo bookingBasicInfoMock;

    private MembershipActivationServiceBean membershipActivationServiceBean;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        setUpMocks();
        ConfigurationEngine.init(this::configure);
    }

    private void configure(Binder binder) {
        binder.bind(MembershipActivationServiceBean.class).toProvider(() -> membershipActivationServiceBeanMock);
        binder.bind(BookingApiManager.class).toProvider(() -> bookingApiManagerMock);
        binder.bind(MemberService.class).toProvider(() -> memberServiceMock);
        binder.bind(MemberAccountService.class).toProvider(() -> memberAccountServiceMock);
        binder.bind(MembershipActivationProducerConfiguration.class).toProvider(() -> membershipActivationProducerConfigurationMock);
        binder.bind(BookingTrackingService.class).toProvider(() -> bookingTrackingServiceMock);
    }

    private void setUpMocks() {
        this.membershipActivationServiceBean = new MembershipActivationServiceBean();
        setUpSubscriptionBookingMock();
        setupBookingDetailMock();
        setUpWebsiteMock();
        setUpBookingBasicInfoMock();
    }

    private void setupBookingDetailMock() {
        setUpSubscriptionBookingMock();
        when(bookingDetailMock.getBookingProducts()).thenReturn(Arrays.asList(new InsuranceBooking(), membershipSubscriptionBookingMock));
        when(bookingDetailMock.getBookingStatus()).thenReturn(TEST_BOOKING_STATUS);
        when(bookingDetailMock.getCollectionState()).thenReturn(ShoppingCartCollectionState.COLLECTED.toString());
        when(bookingDetailMock.getBuyer()).thenReturn(buyer);
        when(buyer.getMail()).thenReturn(TEST_EMAIL);
        when(bookingDetailMock.getUserAgentInfo()).thenReturn(new UserAgentInfo());
        when(bookingDetailMock.getBookingBasicInfo()).thenReturn(bookingBasicInfoMock);
    }

    private void setUpSubscriptionBookingMock() {
        when(membershipSubscriptionBookingMock.getProductType()).thenReturn(PRODUCT_MEMBERSHIP);
        when(membershipSubscriptionBookingMock.getMemberId()).thenReturn(TEST_MEMBERSHIP_ID);
        when(membershipSubscriptionBookingMock.getTotalPrice()).thenReturn(BigDecimal.TEN);
    }

    private void setUpBookingBasicInfoMock() {
        when(bookingBasicInfoMock.getId()).thenReturn(TEST_BOOKING_ID);
        when(bookingBasicInfoMock.getWebsite()).thenReturn(websiteMock);
    }

    private void setUpWebsiteMock() {
        this.websiteMock = mock(Website.class);
        when(websiteMock.getCode()).thenReturn(TEST_WEBSITE);
    }

    @Test
    public void testProcessBookingsDoesCallBookingApiManagerForEachBooking() throws Exception {
        doCallRealMethod().when(membershipActivationServiceBeanMock).processBookingId(TEST_BOOKING_ID);
        assertFalse(membershipActivationServiceBeanMock.processBookingId(TEST_BOOKING_ID));
        verify(bookingApiManagerMock).getBooking(TEST_BOOKING_ID);
    }

    @Test
    public void testProcessBookingsThrowExceptionsWithBookingIds() {
        assertFalse(membershipActivationServiceBean.processBookingId(TEST_BOOKING_ID));
    }

    @Test
    public void testProcessBookingsNotPassingValidation() throws Exception {
        when(bookingDetailMock.getBookingProducts()).thenReturn(Collections.singletonList(new InsuranceBooking()));
        when(bookingDetailMock.getBookingBasicInfo()).thenReturn(bookingBasicInfoMock);
        mockServicesForProcessBookingsWithCustomTestBooking(false, true);
        assertTrue(membershipActivationServiceBeanMock.processBookingId(TEST_BOOKING_ID));
        verify(memberServiceMock, never()).activateMembership(anyLong(), any(BigDecimal.class));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testProcessBookingsThrowsNPEWhenNullDataGiven() {
        membershipActivationServiceBean.processBookingId(null);
    }

    @Test
    public void testProcessBookingsReturnsTrueForTestBooking() throws Exception {
        when(bookingApiManagerMock.getBooking(anyLong())).thenReturn(bookingDetailMock);
        mockServicesForProcessBookingsWithCustomTestBooking(true, false);
        assertTrue(membershipActivationServiceBeanMock.processBookingId(TEST_BOOKING_ID));
        verify(memberServiceMock, never()).activateMembership(anyLong(), any(BigDecimal.class));
    }

    @Test
    public void testProcessBookingsCompliesWithoutTestBooking() throws Exception {
        when(bookingApiManagerMock.getBooking(anyLong())).thenReturn(bookingDetailMock);
        when(memberServiceMock.getMembershipById(anyLong())).thenReturn(assembleMembership());
        mockServicesForProcessBookingsWithCustomTestBooking(false, true);
        assertTrue(membershipActivationServiceBeanMock.processBookingId(TEST_BOOKING_ID));
        verify(memberServiceMock).activateMembership(anyLong(), any(BigDecimal.class));
    }

    @Test
    public void testProcessBookingIdActivateMembershipFails() throws Exception {
        when(bookingDetailMock.getBookingBasicInfo()).thenReturn(bookingBasicInfoMock);
        mockServicesForProcessBookingsWithCustomTestBooking(false, false);
        assertFalse(membershipActivationServiceBeanMock.processBookingId(TEST_BOOKING_ID));
    }

    private void mockServicesForProcessBookingsWithCustomTestBooking(final boolean testBooking, final boolean activateMembership) throws DataAccessException, BookingApiException, MissingElementException, MessageDataAccessException {
        doCallRealMethod().when(membershipActivationServiceBeanMock).processBookingId(anyLong());
        when(membershipActivationServiceBeanMock.getMembershipSubscriptionFromBooking(bookingDetailMock)).thenReturn(Optional.of(membershipSubscriptionBookingMock));
        when(memberServiceMock.activateMembership(anyLong(), any(BigDecimal.class))).thenReturn(activateMembership);
        when(bookingDetailMock.isTestBooking()).thenReturn(testBooking);
        when(bookingApiManagerMock.getBooking(anyLong())).thenReturn(bookingDetailMock);
        when(memberAccountServiceMock.getMemberAccountByMembershipId(anyLong())).thenReturn(new MemberAccount(TEST_MEMBER_ACCOUNT_ID, TEST_USER_ID, NAME, LASTNAME));
    }

    @Test
    public void testProcessBookingsCompliesWithTestBookingAndMember() throws Exception {
        when(bookingDetailMock.getBookingBasicInfo()).thenReturn(bookingBasicInfoMock);
        mockServicesForProcessBookingsWithCustomTestBooking(false, true);
        assertTrue(membershipActivationServiceBeanMock.processBookingId(TEST_BOOKING_ID));
    }

    private Membership assembleMembership() {
        return new MembershipBuilder().setId(TEST_MEMBERSHIP_ID).setWebsite(TEST_WEBSITE).setStatus(TEST_STATUS)
                .setMembershipRenewal(MembershipRenewal.ENABLED).setMemberAccountId(TEST_MEMBER_ACCOUNT_ID)
                .setMonthsDuration(TEST_DEFAULT_DURATION).setMembershipType(MEMBERSHIP_TYPE).setProductStatus(ProductStatus.CONTRACT)
                .setActivationDate(LocalDateTime.now()).setExpirationDate(LocalDateTime.now().plusMonths(12)).build();
    }

    @Test
    public void testGetMemberIdFromBookingWhenNoMembershipSubscriptionAvailable() {
        testGetMemberIdFromBookingWithCustomParams(Arrays.asList(new ProductCategoryBooking[]{}));
    }

    @Test
    public void testGetMembershipSubscriptionAvailable() {
        testGetMemberIdFromBookingWithCustomParams(Arrays.asList(new InsuranceBooking(), membershipSubscriptionBookingMock));
    }

    @Test
    public void testDataAccessExceptionRetrievingBookings() throws Exception {
        when(bookingApiManagerMock.getBooking(anyLong())).thenThrow(new BookingApiException(new Exception()));
        assertFalse(membershipActivationServiceBean.processBookingId(TEST_BOOKING_ID));
    }

    @Test
    public void testGetMemberIdFromBookingWithNullBookingDetail() {
        testGetMemberIdFromBookingWithCustomParams(null);
    }

    @Test
    public void testProcessBookingsBookingApiThrowsException() throws Exception {
        when(bookingApiManagerMock.getBooking(anyLong())).thenThrow(new BookingApiException("Booking Api Test Exception"));
        doCallRealMethod().when(membershipActivationServiceBeanMock).processBookingId(anyLong());
        assertFalse(membershipActivationServiceBeanMock.processBookingId(TEST_BOOKING_ID));
        verify(memberServiceMock, never()).activateMembership(anyLong(), any(BigDecimal.class));
    }

    @Test
    public void testProcessBookingsMemberServiceThrowingDataAccessException() throws Exception {
        mockServicesForProcessBookingsWithCustomTestBooking(false, true);
        when(memberServiceMock.activateMembership(anyLong(), any(BigDecimal.class))).thenThrow(new DataAccessException("Test Exception"));
        assertFalse(membershipActivationServiceBeanMock.processBookingId(TEST_BOOKING_ID));
    }

    private void testGetMemberIdFromBookingWithCustomParams(final List<ProductCategoryBooking> productCategoryBookings) {
        when(membershipActivationServiceBeanMock.getMembershipSubscriptionFromBooking(bookingDetailMock)).thenCallRealMethod();
        when(bookingDetailMock.getBookingProducts()).thenReturn(productCategoryBookings);
        Optional<MembershipSubscriptionBooking> optionalOfMembershipSubscriptionFromBooking = membershipActivationServiceBeanMock.getMembershipSubscriptionFromBooking(bookingDetailMock);
        if (CollectionUtils.isEmpty(productCategoryBookings)) {
            assertFalse(optionalOfMembershipSubscriptionFromBooking.isPresent());
        } else {
            assertTrue(optionalOfMembershipSubscriptionFromBooking.isPresent());
            assertTrue(productCategoryBookings.contains(optionalOfMembershipSubscriptionFromBooking.get()));
        }
    }
}
