package com.odigeo.membership.robots.tracker;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.robots.activator.MockBasicMessageConsumerIterator;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.MessageParserException;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Appender;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertTrue;

public class MembershipTrackerConsumerWorkerTest {

    private static final String BASICMESSAGE_KEY = "1";
    private MembershipTrackerConsumerWorker membershipTrackerConsumerWorker;
    @Mock
    private BookingTrackingService bookingTrackingService;
    private ConsumerIterator<BasicMessage> consumerIterator;
    @Mock
    private Appender mockAppender;
    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        Logger.getRootLogger().addAppender(mockAppender);
    }

    @AfterMethod
    public void tearDownLogger() {
        Logger.getRootLogger().removeAppender(mockAppender);
    }

    private void configure(Binder binder) {
        binder.bind(BookingTrackingService.class).toInstance(bookingTrackingService);
    }

    @Test
    public void testRunExecution() throws Exception {
        membershipTrackerConsumerWorker = createInstance();
        membershipTrackerConsumerWorker.run();
        verify(consumerIterator).next();
        verify(bookingTrackingService, timeout(100)).processBooking(anyLong());
    }

    @Test
    public void testMessageDataAccessException() throws Exception {
        ConsumerIterator<BasicMessage> mockConsumer = mock(ConsumerIterator.class);
        MessageDataAccessException messageDataAccessException = new MessageDataAccessException(StringUtils.EMPTY, new Exception());
        when(mockConsumer.hasNext()).thenThrow(messageDataAccessException);
        membershipTrackerConsumerWorker = new MembershipTrackerConsumerWorker(mockConsumer);
        membershipTrackerConsumerWorker.run();
        verify(mockConsumer).hasNext();
        verify(mockConsumer, never()).next();
        verifyLogs("Error accessing Kafka");
    }

    @Test
    public void testMessageParserException() throws Exception {
        ConsumerIterator<BasicMessage> mockConsumer = mock(ConsumerIterator.class);
        when(mockConsumer.hasNext()).thenReturn(true).thenReturn(false);
        MessageParserException messageParserException = new MessageParserException(StringUtils.EMPTY, new Exception(), StringUtils.EMPTY);
        when(mockConsumer.next()).thenThrow(messageParserException);
        membershipTrackerConsumerWorker = new MembershipTrackerConsumerWorker(mockConsumer);
        membershipTrackerConsumerWorker.run();
        verify(mockConsumer).next();
        verify(bookingTrackingService, never()).processBooking(anyLong());
        verifyLogs("Error parsing message from Kafka before tracking");
    }

    private MembershipTrackerConsumerWorker createInstance() {
        consumerIterator = new MockBasicMessageConsumerIterator(BASICMESSAGE_KEY);
        consumerIterator = spy(consumerIterator);
        return new MembershipTrackerConsumerWorker(consumerIterator);
    }

    private void verifyLogs(String logMessage) {
        verify(mockAppender, timeout(100).atLeastOnce()).doAppend(captorLoggingEvent.capture());
        List<LoggingEvent> loggingEvents = captorLoggingEvent.getAllValues();
        assertTrue(loggingEvents.stream()
                .anyMatch(loggingEvent -> loggingEvent.getRenderedMessage().contains(logMessage)));
    }
}
