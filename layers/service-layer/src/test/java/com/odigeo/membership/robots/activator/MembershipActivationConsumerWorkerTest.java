package com.odigeo.membership.robots.activator;

import com.odigeo.membership.retry.MembershipActivationRetryService;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.MessageDataAccessException;
import org.apache.commons.lang.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.verification.VerificationMode;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

@Test
public class MembershipActivationConsumerWorkerTest {

    private static final int SLEEP_TIME_MILLIS = 500;
    private static final long ASYNC_FUTURE_EXECUTION_TIMEOUT_MS = 100;
    private static final VerificationMode INVOCATIONS_EXPECTED = Mockito.times(3);

    private ConsumerIterator<BasicMessage> consumerIterator;
    @Mock
    private MembershipActivationRetryService membershipActivationRetryService;
    @Mock
    private MembershipActivationService membershipActivationService;
    @Mock
    private BookingTrackingService bookingTrackingService;

    @InjectMocks
    private MembershipActivationConsumerWorker membershipActivationConsumerWorker;

    public static MembershipActivationConsumerWorker createConsumerWorker(ConsumerIterator<BasicMessage> consumerIterator,
                                                                          MembershipActivationService membershipActivationService,
                                                                          MembershipActivationRetryService membershipActivationRetryService,
                                                                          BookingTrackingService bookingTrackingService) {
        return new MembershipActivationConsumerWorker(consumerIterator, membershipActivationService, membershipActivationRetryService, bookingTrackingService);
    }

    @BeforeMethod
    public void setUp() {
        consumerIterator = new MockBasicMessageConsumerIterator("2", "5", "8");
        membershipActivationConsumerWorker = createConsumerWorker(consumerIterator, membershipActivationService, membershipActivationRetryService, bookingTrackingService);
        MockitoAnnotations.initMocks(this);
    }

    public void testRun() {
        membershipActivationConsumerWorker.run();
        verify(membershipActivationService, INVOCATIONS_EXPECTED).processBookingId(Matchers.anyLong());
    }

    public void testSlowRunNotInterrupted() {
        doAnswer(x -> {
            Thread.sleep(SLEEP_TIME_MILLIS);
            return x;
        }).when(membershipActivationService).processBookingId(anyLong());
        membershipActivationConsumerWorker.run();
        verify(membershipActivationService, INVOCATIONS_EXPECTED).processBookingId(Matchers.anyLong());
    }

    public void testRunWithoutBookingIds() {
        MembershipActivationConsumerWorker bookingUpdConsumerWorker = createConsumerWorker(new MockBasicMessageConsumerIterator(), membershipActivationService, membershipActivationRetryService, bookingTrackingService);
        bookingUpdConsumerWorker.run();
        verifyZeroInteractions(membershipActivationService);
    }

    public void testRunWithExceptions() throws Exception {
        ConsumerIterator<BasicMessage> mockConsumer = Mockito.mock(ConsumerIterator.class);
        when(mockConsumer.hasNext()).thenReturn(true);
        MessageDataAccessException messageDataAccessException = new MessageDataAccessException(StringUtils.EMPTY, new Exception());
        when(mockConsumer.next()).thenThrow(messageDataAccessException);
        membershipActivationConsumerWorker = createConsumerWorker(mockConsumer, membershipActivationService, membershipActivationRetryService, bookingTrackingService);
        membershipActivationConsumerWorker.run();
        verifyZeroInteractions(membershipActivationService);
    }

    public void testRunWithRuntimeExceptions() {
        doThrow(new RuntimeException()).when(membershipActivationService).processBookingId(anyLong());
        membershipActivationConsumerWorker.run();
        verify(membershipActivationRetryService, INVOCATIONS_EXPECTED).scheduleForRetry(anyLong());
    }

    public void testRunWithMembershipActivationFails() {
        when(membershipActivationService.processBookingId(anyLong())).thenReturn(false);
        membershipActivationConsumerWorker.run();
        verify(membershipActivationRetryService, INVOCATIONS_EXPECTED).scheduleForRetry(anyLong());
    }

    public void testEquals() {
        MembershipActivationConsumerWorker consumerWorker = createConsumerWorker(consumerIterator, membershipActivationService, membershipActivationRetryService, bookingTrackingService);
        assertEquals(consumerWorker, membershipActivationConsumerWorker);
        assertEquals(membershipActivationConsumerWorker, membershipActivationConsumerWorker);
        assertNotEquals(consumerWorker, null);
        assertNotEquals(null, consumerWorker);
        MembershipActivationConsumerWorker consumerWorkerWithOneId = createConsumerWorker(new MockBasicMessageConsumerIterator("1"), membershipActivationService, membershipActivationRetryService, bookingTrackingService);
        assertNotEquals(consumerWorker, consumerWorkerWithOneId);
        assertNotEquals(StringUtils.EMPTY, consumerWorker);
    }

    public void successfulProcessingTriggersTracking() throws Exception {
        when(membershipActivationService.processBookingId(2L)).thenReturn(true);
        when(membershipActivationService.processBookingId(5L)).thenReturn(true);
        when(membershipActivationService.processBookingId(8L)).thenReturn(false);
        membershipActivationConsumerWorker.run();
        MILLISECONDS.sleep(ASYNC_FUTURE_EXECUTION_TIMEOUT_MS);
        verify(bookingTrackingService).processBooking(2L);
        verify(bookingTrackingService).processBooking(5L);
        verify(bookingTrackingService, never()).processBooking(8L);
    }
}
