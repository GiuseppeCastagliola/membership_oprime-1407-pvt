package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingService;
import com.odigeo.messaging.MembershipSubscriptionMessageService;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class BackOfficeServiceBeanTest {

    private static final String SITE_ES = "ES";
    private static final String EMAIL = "test@edreams.com";
    private static final Long USER_ID = 765L;
    private static final Long MEMBERSHIP_ID = 123L;
    private static final Long MEMBER_ACCOUNT_ID = 443L;
    private static final LocalDateTime ACTIVATION_DATE = LocalDateTime.now();
    private static final LocalDateTime EXPIRATION_DATE = LocalDateTime.now().plusMonths(6);
    private static final MemberAccount MEMBER_ACCOUNT = new MemberAccount(MEMBER_ACCOUNT_ID, USER_ID, "", "");

    @Mock
    private Membership membershipMock;
    @Mock
    private MemberService memberServiceMock;
    @Mock
    private MembershipMessageSendingService membershipMessageSendingServiceMock;
    @Mock
    private MembershipSubscriptionMessageService membershipSubscriptionMessageService;

    @InjectMocks
    private BackOfficeServiceBean backOfficeServiceBean = new BackOfficeServiceBean();

    @Captor
    private ArgumentCaptor<MembershipSubscriptionMessage> membershipSubscriptionMessageCaptor;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
    }

    private void configure(Binder binder) {
        binder.bind(MemberService.class).toInstance(memberServiceMock);
        binder.bind(MembershipMessageSendingService.class).toInstance(membershipMessageSendingServiceMock);
        binder.bind(MembershipSubscriptionMessageService.class).toInstance(membershipSubscriptionMessageService);
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testUpdateMembershipMarketingInfoMemberNotFound() throws MissingElementException, DataAccessException {
        // Given
        when(memberServiceMock.getMembershipByIdWithMemberAccount(MEMBERSHIP_ID)).thenReturn(null);
        // When
        backOfficeServiceBean.updateMembershipMarketingInfo(MEMBERSHIP_ID);
    }

    @Test
    public void testUpdateMembershipMarketingInfoMemberActivated() throws MissingElementException, DataAccessException {
        // Given
        SubscriptionStatus expectedSubscriptionStatus = SubscriptionStatus.SUBSCRIBED;
        mockMembership(MemberStatus.ACTIVATED);
        when(memberServiceMock.getMembershipByIdWithMemberAccount(MEMBERSHIP_ID)).thenReturn(membershipMock);
        when(membershipMessageSendingServiceMock.sendCompletedMembershipSubscriptionMessage(any(MembershipSubscriptionMessage.class))).thenReturn(true);
        mockMessageService(expectedSubscriptionStatus);
        // When
        boolean messageSent = backOfficeServiceBean.updateMembershipMarketingInfo(MEMBERSHIP_ID);
        // Then
        assertTrue(messageSent);
        verifyMessageValues(expectedSubscriptionStatus);
    }

    private void mockMessageService(SubscriptionStatus subscriptionStatus) throws DataNotFoundException {
        MembershipSubscriptionMessage subscriptionMessage = new MembershipSubscriptionMessage.Builder(EMAIL, SITE_ES)
                .withDates(ACTIVATION_DATE.toLocalDate(), EXPIRATION_DATE.toLocalDate())
                .withSubscriptionStatus(subscriptionStatus).build();
        when(membershipSubscriptionMessageService.getMembershipSubscriptionMessage(membershipMock, subscriptionStatus))
                .thenReturn(subscriptionMessage);
    }

    @Test
    public void testUpdateMembershipMarketingInfoMemberDeactivated() throws MissingElementException, DataAccessException {
        // Given
        SubscriptionStatus expectedSubscriptionStatus = SubscriptionStatus.UNSUBSCRIBED;
        mockMembership(MemberStatus.DEACTIVATED);
        when(memberServiceMock.getMembershipByIdWithMemberAccount(MEMBERSHIP_ID)).thenReturn(membershipMock);
        when(membershipMessageSendingServiceMock.sendCompletedMembershipSubscriptionMessage(any(MembershipSubscriptionMessage.class))).thenReturn(true);
        mockMessageService(expectedSubscriptionStatus);
        // When
        boolean messageSent = backOfficeServiceBean.updateMembershipMarketingInfo(MEMBERSHIP_ID);
        // Then
        assertTrue(messageSent);
        verifyMessageValues(expectedSubscriptionStatus);
    }

    private void verifyMessageValues(SubscriptionStatus subscriptionStatus) {
        verify(membershipMessageSendingServiceMock).sendCompletedMembershipSubscriptionMessage(membershipSubscriptionMessageCaptor.capture());
        MembershipSubscriptionMessage subscriptionMessage = membershipSubscriptionMessageCaptor.getValue();
        assertEquals(subscriptionMessage.getEmail(), EMAIL);
        assertEquals(subscriptionMessage.getWebsite(), membershipMock.getWebsite());
        assertEquals(subscriptionMessage.getActivationDate(), membershipMock.getActivationDate().toLocalDate());
        assertEquals(subscriptionMessage.getExpirationDate(), membershipMock.getExpirationDate().toLocalDate());
        assertEquals(subscriptionMessage.getSubscriptionStatus(), subscriptionStatus);
    }

    private void mockMembership(MemberStatus status) {
        when(membershipMock.getStatus()).thenReturn(status);
        when(membershipMock.getWebsite()).thenReturn(SITE_ES);
        when(membershipMock.getActivationDate()).thenReturn(ACTIVATION_DATE);
        when(membershipMock.getExpirationDate()).thenReturn(EXPIRATION_DATE);
        when(membershipMock.getMemberAccount()).thenReturn(MEMBER_ACCOUNT);
    }
}
