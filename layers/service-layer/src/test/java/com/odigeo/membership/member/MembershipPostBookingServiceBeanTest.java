package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.PostBookingPageInfo;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.exception.ActivatedMembershipException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.userarea.MemberUserAreaService;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.messaging.MembershipMessageSendingService;
import com.odigeo.util.CrmCipher;
import org.apache.commons.lang.StringUtils;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static com.odigeo.membership.member.PostBookingParser.CHANNEL_EMAIL;
import static com.odigeo.membership.member.PostBookingParser.DEFAULT_MONTHS_TO_RENEWAL;
import static com.odigeo.membership.member.PostBookingParser.TOKEN;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipPostBookingServiceBeanTest {
    @Mock
    private DataSource dataSource;
    @Mock
    private MemberService memberServiceBean;
    @Mock
    private MemberManager memberManager;
    @Mock
    private MemberUserAreaService memberUserAreaService;
    @Mock
    private MemberStatusActionStore memberStatusActionStore;
    @Mock
    private MemberAccountStore memberAccountStore;
    @Mock
    private CrmCipher crmCipher;
    @Mock
    private MembershipMessageSendingService membershipMessageSendingServiceMock;

    @Captor
    private ArgumentCaptor<MembershipCreation> membershipCreationArgumentCaptor;

    @InjectMocks
    private MembershipPostBookingServiceBean postBookingProcessServiceBean = new MembershipPostBookingServiceBean();

    private static final String EMAIL = "optimus.prime_123@edreams.com";
    private static final String WRONG_EMAIL = "optimus@.com";
    private static final String USER_ID = "42156";
    private static final String NAME = "Tést";
    private static final String LAST_NAMES = "PöstBoòking";
    private static final String WEBSITE_FR = "FR";
    private static final String WEBSITE_GB = "GB";
    private static final String MEMBERSHIP_BASIC_TYPE = MembershipType.BASIC.toString();
    private static final String POST_BOOKING_TOKEN = "@bookingId=123456&email=user@mail.com";
    private static final String POST_BOOKING_BAD_TOKEN = "@bookingIds=123456&email=user@mail.com";
    private static final long BOOKING_ID = 123456L;
    private static final String PB_EMAIL = "user@mail.com";

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configureBinders);
    }

    private void configureBinders(Binder binder) {
        binder.bind(MemberService.class).toInstance(memberServiceBean);
        binder.bind(MemberManager.class).toInstance(memberManager);
        binder.bind(MemberUserAreaService.class).toInstance(memberUserAreaService);
        binder.bind(MemberStatusActionStore.class).toInstance(memberStatusActionStore);
        binder.bind(MemberAccountStore.class).toInstance(memberAccountStore);
        binder.bind(CrmCipher.class).toInstance(crmCipher);
        binder.bind(MembershipMessageSendingService.class).toInstance(membershipMessageSendingServiceMock);
    }

    @Test
    public void testParseInfoAndCreateAccountNullParam() {
        List<String> errors = postBookingProcessServiceBean.parseInfoAndCreateAccounts(null, null, false);
        assertFalse(errors.isEmpty());
        assertEquals(errors.get(0), "No content in the file");
    }

    @Test
    public void testParseInfoAndCreateAccountLessColumnsExpected() {
        String wrongCsv = "USER_ID;NAME;lastName;WEBSITE_FR;monthsToRenewal;\r\n112;test;tést;";
        assertFalse(postBookingProcessServiceBean.parseInfoAndCreateAccounts(wrongCsv, StringUtils.EMPTY, false).isEmpty());
    }

    @Test
    public void testParseInfoAndCreateAccountInternalEmployeeOk() throws DataAccessException {
        parseInfoAndCreateInternalEmployeeAccount();
        verify(memberManager).createMember(eq(dataSource), membershipCreationArgumentCaptor.capture());
        assertEquals(String.valueOf(membershipCreationArgumentCaptor.getValue().getUserId()), USER_ID);
        assertEquals(membershipCreationArgumentCaptor.getValue().getName(), NAME);
        assertEquals(membershipCreationArgumentCaptor.getValue().getLastNames(), LAST_NAMES);
        assertEquals(membershipCreationArgumentCaptor.getValue().getWebsite(), WEBSITE_GB);
        assertEquals(membershipCreationArgumentCaptor.getValue().getStatusAction().toString(), StatusAction.INTERNAL_CREATION.toString());
        assertEquals(membershipCreationArgumentCaptor.getValue().getMembershipType().toString(), MembershipType.BASIC.toString());
    }

    @Test
    public void testParseInfoAndCreateAccountPostBookingDefaultMonthsToRenewalOk() throws DataAccessException {
        parseInfoAndCreatePostBookingAccount(StringUtils.EMPTY, EMAIL, StringUtils.EMPTY, MEMBERSHIP_BASIC_TYPE);
        verify(memberManager).createMember(eq(dataSource), membershipCreationArgumentCaptor.capture());
        verifyArgumentsOnMembershipCreation(membershipCreationArgumentCaptor, StatusAction.PB_PHONE_CREATION.toString(), MEMBERSHIP_BASIC_TYPE);
    }

    @Test
    public void testParseInfoAndCreateBusinessAccountPostBookingOk() throws DataAccessException {
        MembershipType businessMembershipType = MembershipType.BUSINESS;
        parseInfoAndCreatePostBookingAccount(StringUtils.EMPTY, EMAIL, StringUtils.EMPTY, businessMembershipType.toString());
        verify(memberManager).createMember(eq(dataSource), membershipCreationArgumentCaptor.capture());
        verifyArgumentsOnMembershipCreation(membershipCreationArgumentCaptor, StatusAction.PB_PHONE_CREATION.toString(), businessMembershipType.toString());
    }

    @Test
    public void testParseInfoAndCreateAccountPostBookingDefaultChannelEmail() throws DataAccessException {
        parseInfoAndCreatePostBookingAccount(StringUtils.EMPTY, EMAIL, CHANNEL_EMAIL, MEMBERSHIP_BASIC_TYPE);
        verify(memberManager).createMember(eq(dataSource), membershipCreationArgumentCaptor.capture());
        verifyArgumentsOnMembershipCreation(membershipCreationArgumentCaptor, StatusAction.PB_EMAIL_CREATION.toString(), MEMBERSHIP_BASIC_TYPE);
    }

    private void verifyArgumentsOnMembershipCreation(ArgumentCaptor<MembershipCreation> argument, String expectedStatusAction, String expectedMembershipType) {
        assertEquals(String.valueOf(argument.getValue().getUserId()), USER_ID);
        assertEquals(argument.getValue().getName(), NAME);
        assertEquals(argument.getValue().getLastNames(), LAST_NAMES);
        assertEquals(argument.getValue().getWebsite(), WEBSITE_FR);
        assertEquals(LocalDate.now().plusMonths(DEFAULT_MONTHS_TO_RENEWAL), argument.getValue().getExpirationDate());
        assertEquals(argument.getValue().getStatusAction().toString(), expectedStatusAction);
        assertEquals(argument.getValue().getMembershipType().toString(), expectedMembershipType);
    }

    @Test
    public void testParseInfoAndCreateAccountPostBookingMonthsWrongFormatKO() throws DataAccessException {
        List<String> errors = parseInfoAndCreatePostBookingAccount("-1", EMAIL, StringUtils.EMPTY, MEMBERSHIP_BASIC_TYPE);
        assertFalse(errors.isEmpty());
    }

    @Test
    public void testParseInfoAndCreateAccountPostBookingMonthsExceedMaximumKO() throws DataAccessException {
        List<String> errors = parseInfoAndCreatePostBookingAccount("18", EMAIL, StringUtils.EMPTY, MEMBERSHIP_BASIC_TYPE);
        assertFalse(errors.isEmpty());
    }

    @Test
    public void testParseInfoAndCreateAccountPostBookingThreeMonthsTrialOk() throws DataAccessException {
        int monthsToRenewal = 3;
        ArgumentCaptor<MembershipCreation> argument = ArgumentCaptor.forClass(MembershipCreation.class);
        parseInfoAndCreatePostBookingAccount(String.valueOf(monthsToRenewal), EMAIL, StringUtils.EMPTY, MEMBERSHIP_BASIC_TYPE);
        verify(memberManager).createMember(eq(dataSource), argument.capture());
        verifyArgumentsOnPostBookingAccountCreation(argument, monthsToRenewal);
    }

    @Test
    public void testParseInfoAndCreateAccountPostBookingEmptyEmail() throws DataAccessException {
        List<String> errors = parseInfoAndCreatePostBookingAccount("12", StringUtils.EMPTY, StringUtils.EMPTY, MEMBERSHIP_BASIC_TYPE);
        assertFalse(errors.isEmpty());
        assertTrue(errors.get(0).contains("attribute empty"));
    }

    @Test
    public void testParseInfoAndCreateAccountPostBookingWrongEmail() throws DataAccessException {
        List<String> errors = parseInfoAndCreatePostBookingAccount("12", WRONG_EMAIL, StringUtils.EMPTY, MEMBERSHIP_BASIC_TYPE);
        assertFalse(errors.isEmpty());
        assertTrue(errors.get(0).contains("Wrong email format"));
    }

    @Test
    public void testCreateMembershipSubscriptionPostBookingOK()
            throws DataAccessException, SQLException, BuilderException, ActivatedMembershipException, BookingApiException {
        MembershipCreation membershipCreation = getMembershipCreation(true);
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(444L);
        when(memberUserAreaService.getBookingDetailSubscription(anyLong())).thenReturn(new BookingDetailBuilder().build(new Random()));
        postBookingProcessServiceBean.createPostBookingMembership(membershipCreation, EMAIL);
        verify(memberStatusActionStore).createMemberStatusAction(eq(dataSource), anyLong(), any(StatusAction.class));
        verify(membershipMessageSendingServiceMock).sendMembershipSubscriptionMessage(any());
        verify(memberStatusActionStore).createMemberStatusAction(eq(dataSource), anyLong(), any(StatusAction.class));
    }

    @Test
    public void testCreatePostBookingMembershipBasicFree() throws DataAccessException, ActivatedMembershipException {
        MembershipCreation membershipCreation = getMembershipCreation(true);
        membershipCreation.setMembershipType(MembershipType.BASIC_FREE);
        postBookingProcessServiceBean.createPostBookingMembership(membershipCreation, EMAIL);
        verify(memberManager).createMember(eq(dataSource), membershipCreationArgumentCaptor.capture());
        assertEquals(membershipCreationArgumentCaptor.getValue().getAutoRenewal(), MembershipRenewal.DISABLED);
    }

    @Test(expectedExceptions = ActivatedMembershipException.class, expectedExceptionsMessageRegExp = "An activated membership exists for userId 333 and website ES")
    public void testCreateMembershipSubscriptionPostBookingUserAccountKO()
            throws SQLException, ActivatedMembershipException, DataAccessException {
        MemberAccount memberAccount = getMemberAccountWithMembership();
        List<MemberAccount> memberAccountList = Collections.singletonList(memberAccount);
        when(memberAccountStore.getMemberAccountAndMembershipsByStatus(dataSource, 333L, MemberStatus.ACTIVATED)).thenReturn(memberAccountList);
        postBookingProcessServiceBean.createPostBookingMembership(getMembershipCreation(true), EMAIL);
    }

    @Test(expectedExceptions = DataAccessException.class, expectedExceptionsMessageRegExp = "Error creating membership for userId 333")
    public void testCreateMembershipSubscriptionPostBookingSQLExceptionKO()
            throws SQLException, ActivatedMembershipException, DataAccessException {
        when(memberAccountStore.getMemberAccountAndMembershipsByStatus(dataSource, 333L, MemberStatus.ACTIVATED)).thenThrow(new SQLException(StringUtils.EMPTY));
        postBookingProcessServiceBean.createPostBookingMembership(getMembershipCreation(true), EMAIL);
    }

    @Test
    public void testCreatePostBookingMembershipPending() throws DataAccessException, BuilderException,
            ActivatedMembershipException, SQLException, BookingApiException {
        MembershipCreation membershipCreation = getMembershipCreation(false);
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(444L);
        when(memberUserAreaService.getBookingDetailSubscription(anyLong())).thenReturn(new BookingDetailBuilder().build(new Random()));
        postBookingProcessServiceBean.createPostBookingMembershipPending(membershipCreation);
        verify(memberStatusActionStore, times(1)).createMemberStatusAction(eq(dataSource), eq(444L), eq(StatusAction.PB_PHONE_CREATION));
    }

    @Test(expectedExceptions = ActivatedMembershipException.class, expectedExceptionsMessageRegExp = "An activated membership exists for userId 333 and website ES")
    public void testCreatePendingMembershipSubscriptionPostBookingAlreadyActivatedKO() throws SQLException, ActivatedMembershipException, DataAccessException {
        List<MemberAccount> memberAccountList = Collections.singletonList(getMemberAccountWithMembership());
        when(memberAccountStore.getMemberAccountAndMembershipsByStatus(dataSource, 333L, MemberStatus.ACTIVATED)).thenReturn(memberAccountList);
        postBookingProcessServiceBean.createPostBookingMembershipPending(getMembershipCreation(false));
    }

    @Test(expectedExceptions = DataAccessException.class, expectedExceptionsMessageRegExp = "Error creating membership for userId 333")
    public void testCreatePendingMembershipSubscriptionPostBookingSQLExceptionKO() throws SQLException, ActivatedMembershipException, DataAccessException {
        when(memberAccountStore.getMemberAccountAndMembershipsByStatus(dataSource, 333L, MemberStatus.ACTIVATED)).thenThrow(new SQLException(StringUtils.EMPTY));
        postBookingProcessServiceBean.createPostBookingMembershipPending(getMembershipCreation(false));
    }

    @Test(expectedExceptions = DataAccessException.class, expectedExceptionsMessageRegExp = "Error creating memberStatusAction PB_PHONE_CREATION for membershipId 0")
    public void testCreatePendingMembershipSubscriptionPostBookingStatusActionSQLExceptionKO() throws SQLException, ActivatedMembershipException, DataAccessException {
        doThrow(new SQLException(StringUtils.EMPTY)).when(memberStatusActionStore).createMemberStatusAction(eq(dataSource), anyLong(), eq(StatusAction.PB_PHONE_CREATION));
        postBookingProcessServiceBean.createPostBookingMembershipPending(getMembershipCreation(false));
    }

    @Test
    public void testGetPostBookingPageInfo() throws Exception {
        when(crmCipher.decryptToken(anyString())).thenReturn(POST_BOOKING_TOKEN);
        PostBookingPageInfo postBookingPageInfo = postBookingProcessServiceBean.getPostBookingPageInfo("token");
        PostBookingPageInfo idealPostBookingPageInfo = new PostBookingPageInfo(BOOKING_ID, PB_EMAIL);
        assertEquals(postBookingPageInfo, idealPostBookingPageInfo);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = PostBookingPageInfo.EXCEPTION_MESSAGE)
    public void testGetPostBookingPageInfoShouldThrowExceptionWithWrongToken() throws Exception {
        when(crmCipher.decryptToken(anyString())).thenReturn(POST_BOOKING_BAD_TOKEN);
        postBookingProcessServiceBean.getPostBookingPageInfo("token");
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testGetPostBookingPageInfoWithException() throws Exception {
        when(crmCipher.decryptToken(anyString())).thenThrow(new IllegalArgumentException());
        postBookingProcessServiceBean.getPostBookingPageInfo("badToken");
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = PostBookingPageInfo.EXCEPTION_MESSAGE)
    public void testGetPostBookingPageInfoWithEmptyToken() throws Exception {
        when(crmCipher.decryptToken(anyString())).thenReturn("NOT_READABLE");
        postBookingProcessServiceBean.getPostBookingPageInfo("");
    }

    @Test
    public void testCreateMembershipSubscriptionPostBooking_SendCorrectIdToReporter()
            throws DataAccessException, BuilderException, ActivatedMembershipException,
            BookingApiException {
        MembershipCreation membershipCreation = getMembershipCreation(true);
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(444L);
        when(memberUserAreaService.getBookingDetailSubscription(anyLong())).thenReturn(new BookingDetailBuilder().build(new Random()));
        postBookingProcessServiceBean.createPostBookingMembership(membershipCreation, EMAIL);
        verify(membershipMessageSendingServiceMock).sendMembershipIdToMembershipReporter(444L);
    }

    @Test
    public void testCreatePostBookingMembershipPending_SendCorrectIdToReporter() throws DataAccessException, BuilderException,
            ActivatedMembershipException, BookingApiException {
        MembershipCreation membershipCreation = getMembershipCreation(false);
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(444L);
        when(memberUserAreaService.getBookingDetailSubscription(anyLong())).thenReturn(new BookingDetailBuilder().build(new Random()));
        postBookingProcessServiceBean.createPostBookingMembershipPending(membershipCreation);
        verify(membershipMessageSendingServiceMock).sendMembershipIdToMembershipReporter(444L);
    }

    private MemberAccount getMemberAccountWithMembership() {
        MemberAccount memberAccount = new MemberAccount(123L, 333L, "juan", "carlos");
        Membership membership = new MembershipBuilder().setId(555L).setWebsite("ES").setStatus(MemberStatus.ACTIVATED).setMemberAccountId(123L).setProductStatus(ProductStatus.CONTRACT).build();
        memberAccount.setMemberships(Collections.singletonList(membership));
        return memberAccount;
    }

    private MembershipCreation getMembershipCreation(boolean withExpirationDate) {
        MembershipCreation membershipCreation = new MembershipCreationBuilder()
                .withUserId(333L)
                .withName("test")
                .withLastNames("postbooking")
                .withWebsite("ES")
                .withMembershipType(MembershipType.BASIC)
                .withSourceType(SourceType.POST_BOOKING)
                .withSubscriptionPrice(BigDecimal.ZERO)
                .withStatusAction(StatusAction.PB_PHONE_CREATION)
                .build();
        if (withExpirationDate) {
            membershipCreation.setActivationDate(LocalDate.now());
            membershipCreation.setExpirationDate(LocalDate.now().plusMonths(12));
        }
        return membershipCreation;
    }

    private MembershipCreation getPostBookingMembershipCreation(String userId) {
        return new MembershipCreationBuilder()
                .withUserId(Long.parseLong(userId))
                .withName(NAME)
                .withLastNames(LAST_NAMES)
                .withWebsite(WEBSITE_FR)
                .withMembershipType(MembershipType.BASIC)
                .withSourceType(SourceType.POST_BOOKING)
                .withSubscriptionPrice(BigDecimal.ZERO)
                .withStatusAction(StatusAction.PB_PHONE_CREATION)
                .build();
    }

    private void parseInfoAndCreateInternalEmployeeAccount() throws DataAccessException {
        MembershipCreation membershipCreation = getPostBookingMembershipCreation(USER_ID);
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(112L);
        StringBuilder csv = new StringBuilder("USER_ID;NAME;lastName;WEBSITE_FR;\r\n");
        csv.append(USER_ID).append(TOKEN).append(NAME).append(TOKEN).append(LAST_NAMES).append(TOKEN).append(WEBSITE_FR);
        postBookingProcessServiceBean.parseInfoAndCreateAccounts(csv.toString(), StringUtils.EMPTY, true);
    }

    private List<String> parseInfoAndCreatePostBookingAccount(String monthsToRenewal, String email, String channel, String membershipType) throws DataAccessException {
        MembershipCreation membershipCreation = getPostBookingMembershipCreation(USER_ID);
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(112L);
        StringBuilder csv = new StringBuilder("USER_ID;NAME;lastName;WEBSITE_FR;email;monthsToRenewal;membershipType;\r\n");
        csv.append(USER_ID).append(TOKEN).append(NAME).append(TOKEN).append(LAST_NAMES).append(TOKEN)
                .append(WEBSITE_FR).append(TOKEN).append(email).append(TOKEN).append(monthsToRenewal).append(TOKEN).append(membershipType);
        return postBookingProcessServiceBean.parseInfoAndCreateAccounts(csv.toString(), channel, false);
    }

    private void verifyArgumentsOnPostBookingAccountCreation(ArgumentCaptor<MembershipCreation> argument, int monthsToRenewal) {
        assertEquals(String.valueOf(argument.getValue().getUserId()), USER_ID);
        assertEquals(argument.getValue().getName(), NAME);
        assertEquals(argument.getValue().getLastNames(), LAST_NAMES);
        assertEquals(argument.getValue().getWebsite(), WEBSITE_FR);
        assertEquals(argument.getValue().getExpirationDate(), LocalDate.now().plusMonths(monthsToRenewal));
    }
}
