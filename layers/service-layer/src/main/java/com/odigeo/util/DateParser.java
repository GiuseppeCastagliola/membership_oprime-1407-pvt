package com.odigeo.util;

import org.apache.commons.lang.StringUtils;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

public final class DateParser {

    private DateParser() {
    }

    public static String toIsoDateTime(LocalDateTime localDateTime) {
        return Objects.nonNull(localDateTime) ? localDateTime.format(DateTimeFormatter.ISO_DATE_TIME) : StringUtils.EMPTY;
    }

    public static String toIsoDate(LocalDateTime localDateTime) {
        return Objects.nonNull(localDateTime) ? localDateTime.format(DateTimeFormatter.ISO_DATE) : StringUtils.EMPTY;
    }

    public static Optional<String> toIsoDate(final Date dateObject) {
        return Objects.nonNull(dateObject) ? Optional.of(isoDateFormat().format(dateObject)) : Optional.empty();
    }

    private static SimpleDateFormat isoDateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    }
}
