package com.odigeo.userapi;

import com.odigeo.userprofiles.api.v1.model.Brand;

import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

import static org.apache.commons.lang.StringUtils.EMPTY;

public final class BrandMapper {

    private static final String ED_PREFIX = "ED";
    private static final String GO_PREFIX = "GO";
    private static final String OP_PREFIX = "OP";
    private static final String TL_PREFIX = "TL";

    private BrandMapper() {
    }

    public static  Optional<Brand> map(String website) {

        if (Objects.isNull(website)) {
            return Optional.empty();
        }

        String formattedWebsite = website.trim().toUpperCase(Locale.getDefault());

        if (formattedWebsite.length() == 2) {
            return Optional.of(Brand.ED);
        }

        if (formattedWebsite.length() == 4) {
            return findBrandForWebsite(formattedWebsite);
        }

        return Optional.empty();
    }

    private static Optional<Brand> findBrandForWebsite(String formattedWebsite) {
        String prefix = formattedWebsite.substring(0, 2);

        switch (prefix) {
        case EMPTY:
        case ED_PREFIX:
            return Optional.of(Brand.ED);
        case GO_PREFIX:
            return Optional.of(Brand.GV);
        case OP_PREFIX:
            return Optional.of(Brand.OP);
        case TL_PREFIX:
            return Optional.of(Brand.TL);
        default:
            return Optional.empty();
        }
    }
}
