package com.odigeo.membership.robots;

public interface BookingUpdatesReaderService {

    boolean startActivationConsumerController();

    void stopActivationConsumerController();

    boolean startTrackerConsumerController();

    void stopTrackerConsumerController();
}
