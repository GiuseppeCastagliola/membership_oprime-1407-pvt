package com.odigeo.membership.retry;

import com.odigeo.messaging.message.MembershipActivationRetryMessage;

public interface MembershipActivationRetryService {
    Boolean scheduleForRetry(Long bookingId);
    Boolean retryActivation(MembershipActivationRetryMessage message);
}
