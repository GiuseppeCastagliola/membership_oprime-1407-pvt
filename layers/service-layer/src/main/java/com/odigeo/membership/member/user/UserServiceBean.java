package com.odigeo.membership.member.user;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.userapi.BrandMapper;
import com.odigeo.userapi.configuration.UserProfileApiSecurityConfiguration;
import com.odigeo.userprofiles.api.v1.UserApiService;
import com.odigeo.userprofiles.api.v1.model.AppCredentials;
import com.odigeo.userprofiles.api.v1.model.InvalidCredentialsException;
import com.odigeo.userprofiles.api.v1.model.InvalidFindException;
import com.odigeo.userprofiles.api.v1.model.InvalidValidationException;
import com.odigeo.userprofiles.api.v1.model.RemoteException;
import com.odigeo.userprofiles.api.v1.model.StatusUserException;
import com.odigeo.userprofiles.api.v1.model.ThirdAppRequest;
import com.odigeo.userprofiles.api.v1.model.User;
import com.odigeo.userprofiles.api.v1.model.UserRegisteredResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

@Singleton
public class UserServiceBean implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceBean.class);
    private UserApiService userApiService;
    private UserProfileApiSecurityConfiguration userApiSecurityConfig;
    private AppCredentials appCredentials;

    @Override
    public Long saveUser(UserCreation userCreation) throws DataAccessException {
        LOGGER.info("saveUser {}", userCreation);
        UserRegisteredResponse registeredUserInfo = getRegisteredUser(userCreation);
        if (registeredUserInfo.isRegistered()) {
            LOGGER.info("User with id {} and status {} already exists", registeredUserInfo.getId(), registeredUserInfo.getUserStatus());
            return registeredUserInfo.getId();
        }
        User user = UserCreationMapper.mapToUser(userCreation);
        try {
            User createdUser = getUserApiService().saveUser(user);
            LOGGER.info("createdUser with id {}", createdUser.getId());
            return createdUser.getId();
        } catch (InvalidCredentialsException | RemoteException | StatusUserException | InvalidValidationException e) {
            LOGGER.error("Problem occurred saving user {}", user, e);
            throw new DataAccessException("Problem occurred saving user", e);
        }
    }

    private UserRegisteredResponse getRegisteredUser(UserCreation userCreation) throws DataAccessException {
        ThirdAppRequest thirdAppRequest = new ThirdAppRequest();
        thirdAppRequest.setAppCredentials(getAppCredentials());
        thirdAppRequest.setBrand(BrandMapper.map(userCreation.getWebsite()).orElse(null));
        thirdAppRequest.setEmail(userCreation.getEmail());
        try {
            return getUserApiService().isUserRegistered(thirdAppRequest);
        } catch (InvalidValidationException | RemoteException | InvalidFindException | InvalidCredentialsException e) {
            LOGGER.error("Problem checking isUserRegistered {}", userCreation, e);
            throw new DataAccessException("Problem checking isUserRegistered", e);
        }
    }

    private AppCredentials getAppCredentials() {
        if (Objects.isNull(appCredentials)) {
            appCredentials = new AppCredentials();
            appCredentials.setApplicationName(getUserApiSecurityConfig().getUser());
            appCredentials.setPassword(getUserApiSecurityConfig().getPassword());
        }
        return appCredentials;
    }

    private UserProfileApiSecurityConfiguration getUserApiSecurityConfig() {
        if (Objects.isNull(userApiSecurityConfig)) {
            userApiSecurityConfig = ConfigurationEngine.getInstance(UserProfileApiSecurityConfiguration.class);
        }
        return userApiSecurityConfig;
    }

    private UserApiService getUserApiService() {
        if (Objects.isNull(userApiService)) {
            userApiService = ConfigurationEngine.getInstance(UserApiService.class);
        }
        return userApiService;
    }
}
