package com.odigeo.membership.auth.usecases;


import com.odigeo.membership.auth.AuthenticationResponse;
import com.odigeo.membership.auth.exception.MembershipAuthServiceException;

public interface AuthenticationLoginUseCase {
    AuthenticationResponse login(String userName, String password) throws MembershipAuthServiceException;
}
