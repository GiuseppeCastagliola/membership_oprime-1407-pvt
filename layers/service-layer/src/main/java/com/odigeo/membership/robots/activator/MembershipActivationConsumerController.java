package com.odigeo.membership.robots.activator;

import com.google.inject.Inject;
import com.odigeo.membership.retry.MembershipActivationRetryService;
import com.odigeo.membership.retry.MembershipActivationRetryServiceBean;
import com.odigeo.membership.robots.ConsumerController;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.membership.tracking.BookingTrackingServiceBean;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.Consumer;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.kafka.KafkaJsonConsumer;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class MembershipActivationConsumerController implements ConsumerController {

    private static final int NUM_THREADS = 2;
    private static final Logger LOGGER = Logger.getLogger(MembershipActivationConsumerController.class);
    private final Consumer<BasicMessage> consumer;
    private final int numThreads;
    private final ExecutorService executorService;
    private final MembershipActivationService membershipActivationService;
    private final MembershipActivationRetryService membershipActivationRetryService;
    private final BookingTrackingService bookingTrackingService;

    @Inject
    public MembershipActivationConsumerController(BookingUpdatesConsumerConfiguration configuration,
                                                  MembershipActivationService membershipActivationService,
                                                  MembershipActivationRetryService membershipActivationRetryService,
                                                  BookingTrackingService bookingTrackingService) {
        this.membershipActivationService = membershipActivationService;
        this.membershipActivationRetryService = membershipActivationRetryService;
        this.bookingTrackingService = bookingTrackingService;
        consumer = new KafkaJsonConsumer<>(BasicMessage.class, configuration.getZooKeeperUrl(), configuration.getActivatorConsumerGroup(), configuration.getTopicName());
        numThreads = configuration.getNumConsumerThreads();
        executorService = Executors.newFixedThreadPool(numThreads);
    }

    protected MembershipActivationConsumerController(Consumer consumer, ExecutorService executorService) {
        this.consumer = consumer;
        this.numThreads = NUM_THREADS;
        this.executorService = executorService;
        membershipActivationService = new MembershipActivationServiceBean();
        membershipActivationRetryService = new MembershipActivationRetryServiceBean();
        bookingTrackingService = new BookingTrackingServiceBean();
    }

    @Override
    public void startMultiThreadConsumer() {
        List<Future> futures = new ArrayList<>(numThreads);
        List<ConsumerIterator<BasicMessage>> consumerIteratorList = consumer.connectAndIterate(numThreads);
        for (ConsumerIterator<BasicMessage> consumerIterator : consumerIteratorList) {
            futures.add(executorService.submit(new MembershipActivationConsumerWorker(consumerIterator,
                    membershipActivationService,
                    membershipActivationRetryService,
                    bookingTrackingService)));
        }
        for (Future future : futures) {
            try {
                if (future.get() != null) {
                    LOGGER.error("Got a non-null return for future " + future.toString());
                }
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.error("Caught an " + e.getClass().getSimpleName() + " " + e.getMessage(), e);
            }
        }
    }

    @Override
    public void stopMultiThreadConsumer() {
        consumer.close();
        executorService.shutdown();
    }

    public void join() throws InterruptedException {
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
    }

}
