package com.odigeo.membership.auth.usecases;

import com.odigeo.membership.auth.PermissionResponse;
import com.odigeo.membership.auth.exception.MembershipAuthServiceException;

public interface AuthorizationPermissionsUseCase {
    PermissionResponse getTokenPermissions(String token) throws MembershipAuthServiceException;
}
