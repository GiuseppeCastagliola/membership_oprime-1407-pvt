package com.odigeo.membership.robots.activator;

import com.edreams.base.MissingElementException;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.retry.MembershipActivationRetryService;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.MessageParserException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

public class MembershipActivationConsumerWorker implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(MembershipActivationConsumerWorker.class);

    private final ConsumerIterator<BasicMessage> consumerMessageIterator;

    private final MembershipActivationService membershipActivationService;

    private final MembershipActivationRetryService membershipActivationRetryService;
    private final BookingTrackingService bookingTrackingService;


    public MembershipActivationConsumerWorker(ConsumerIterator<BasicMessage> consumerMessageIterator,
                                              MembershipActivationService membershipActivationService,
                                              MembershipActivationRetryService membershipActivationRetryService,
                                              BookingTrackingService bookingTrackingService) {
        this.consumerMessageIterator = consumerMessageIterator;
        this.membershipActivationService = membershipActivationService;
        this.membershipActivationRetryService = membershipActivationRetryService;
        this.bookingTrackingService = bookingTrackingService;
    }

    @Override
    public void run() {
        List<CompletableFuture<Void>> completableFutureList = new ArrayList<>();
        try {
            fillCompletableFutureList(completableFutureList);
            LOGGER.info("Consumer closed for consumer thread!");
        } catch (MessageDataAccessException e) {
            LOGGER.error("Error accessing Kafka", e);
        }
        CompletableFuture.allOf(completableFutureList.toArray(new CompletableFuture[completableFutureList.size()])).join();
    }

    private void fillCompletableFutureList(List completableFutureList) throws MessageDataAccessException {
        while (consumerMessageIterator.hasNext()) {
            try {
                BasicMessage bookingMessage = consumerMessageIterator.next();
                long bookingId = Long.parseLong(bookingMessage.getKey());
                completableFutureList.add(CompletableFuture.runAsync(() -> activate(bookingId))
                        .exceptionally(handleException(bookingId)));
            } catch (MessageParserException e) {
                LOGGER.error("Error parsing message from Kafka", e);
            }
        }
    }

    private void activate(final long bookingId) {
        if (!membershipActivationService.processBookingId(bookingId)) {
            membershipActivationRetryService.scheduleForRetry(bookingId);
        } else {
            CompletableFuture.runAsync(() -> trackBooking(bookingId));
        }
    }

    private void trackBooking(Long bookingId) {
        try {
            bookingTrackingService.processBooking(bookingId);
        } catch (MissingElementException | RetryableException e) {
            LOGGER.error("Failed to track booking " + bookingId + " with error message " + e.getMessage());
        }
    }

    private Function<Throwable, Void> handleException(final long bookingId) {
        return e -> {
            LOGGER.error(e.getClass().getSimpleName() + " during " + bookingId + " activation! Will be rescheduled for retry", e);
            membershipActivationRetryService.scheduleForRetry(bookingId);
            return null;
        };
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MembershipActivationConsumerWorker that = (MembershipActivationConsumerWorker) o;
        return Objects.equals(consumerMessageIterator, that.consumerMessageIterator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(consumerMessageIterator);
    }

}
