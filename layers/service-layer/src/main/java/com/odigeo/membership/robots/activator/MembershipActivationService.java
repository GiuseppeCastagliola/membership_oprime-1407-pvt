package com.odigeo.membership.robots.activator;

public interface MembershipActivationService {

    boolean processBookingId(Long bookingIdToProcess);

}
