package com.odigeo.membership.parameters.search;

import com.odigeo.membership.enums.db.MemberAccountField;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

public class MemberAccountSearch extends SearchQueries {

    private final String name;
    private final String lastNames;
    private final Long userId;
    private final boolean withMemberships;
    private final String query;
    private final List<FilterCondition> filterConditions;

    public MemberAccountSearch(Builder builder) {
        name = builder.name;
        lastNames = builder.lastNames;
        userId = builder.userId;
        withMemberships = builder.withMembership;
        filterConditions = Collections.unmodifiableList(toList());
        query = queryCompiler(filterConditions);
    }

    public List<Object> getValues() {
        return filterConditions.stream()
                .map(FilterCondition::getValue)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isEmpty() {
        return Arrays.stream(new Object[]{name, lastNames, userId}).noneMatch(Objects::nonNull);
    }

    public String getQueryString() {
        return query;
    }

    public boolean isWithMemberships() {
        return withMemberships;
    }

    protected List<FilterCondition> getFilterConditions() {
        return filterConditions;
    }

    public static class Builder {
        private String name;
        private String lastNames;
        private Long userId;
        private boolean withMembership;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder lastNames(String lastNames) {
            this.lastNames = lastNames;
            return this;
        }

        public Builder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public Builder withMembership(boolean withMembership) {
            this.withMembership = withMembership;
            return this;
        }

        public MemberAccountSearch build() {
            return new MemberAccountSearch(this);
        }
    }

    private String queryCompiler(List<FilterCondition> filterConditions) {
        StringBuilder queryBuilder = new StringBuilder(withMemberships ? SEARCH_MEMBERSHIPS_WITH_ACCOUNT : SEARCH_ACCOUNTS)
                .append(SqlKeywords.WHERE)
                .append(filterConditions.stream()
                        .map(FilterCondition::toString)
                        .collect(joining(SqlKeywords.AND.toString())));
        return withMemberships ? queryBuilder.append(SORT_BY_EXPDATE).toString()
                : queryBuilder.toString();
    }

    private List<FilterCondition> toList() {
        List<FilterCondition> conditions = new ArrayList<>();
        addEqualsIfNotNull(conditions, MemberAccountField.USER_ID, userId);
        addEqualsIfNotNull(conditions, MemberAccountField.FIRST_NAME, name);
        addEqualsIfNotNull(conditions, MemberAccountField.LAST_NAME, lastNames);
        return conditions;
    }

}
