package com.odigeo.membership.util;

import com.google.inject.Singleton;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Singleton
public class EncryptUtils {

    public String encode64(byte[] value) {
        return new String(Base64.getEncoder().encode(value), Charset.forName(StandardCharsets.UTF_8.name()));
    }

    public String encode64(String value) {
        return encode64(getBytesUTF8(value));
    }

    public String decode64(String value) {
        return new String(decode64Buffer(value), Charset.forName(StandardCharsets.UTF_8.name()));
    }

    public byte[] decode64Buffer(String value) {
        return Base64.getDecoder().decode(value);
    }

    public String decodeUrlToken(String value) throws UnsupportedEncodingException {
        return new String(decodeUrlTokenBuffer(value), StandardCharsets.UTF_8);
    }

    public byte[] decodeUrlTokenBuffer(String value) throws UnsupportedEncodingException {
        return decode64Buffer(URLDecoder.decode(value, StandardCharsets.UTF_8.name()));
    }

    public String encodeUrlTokenBuffer(byte[] value) throws UnsupportedEncodingException {
        return URLEncoder.encode(encode64(value), StandardCharsets.UTF_8.name());
    }

    public String encodeUrlToken(String value) throws UnsupportedEncodingException {
        return encodeUrlTokenBuffer(getBytesUTF8(value));
    }

    public byte[] getBytesUTF8(String value) {
        return value.getBytes(StandardCharsets.UTF_8);
    }

}
