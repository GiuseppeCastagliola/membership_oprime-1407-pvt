package com.odigeo.membership.activation.validation;

import org.apache.commons.collections.CollectionUtils;

import java.util.List;

public final class ValidationHelper {

    private ValidationHelper() {
    }

    public static boolean runValidations(List<Validation> validations) {
        if (CollectionUtils.isEmpty(validations)) {
            throw new IllegalArgumentException("At least one validation to be run is needed");
        }

        return validations.stream().allMatch(Validation::validate);
    }
}
