package com.odigeo.membership;

import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import org.apache.commons.lang.ObjectUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class MembershipBuilder {

    private Long id;
    private String website;
    private MemberStatus status;
    private MembershipRenewal membershipRenewal;
    private LocalDateTime activationDate;
    private LocalDateTime expirationDate;
    private Boolean isBookingLimitReached;
    private long memberAccountId;
    private BigDecimal balance;
    private MembershipType membershipType;
    private int monthsDuration;
    private SourceType sourceType;
    private ProductStatus productStatus;
    private BigDecimal totalPrice;
    private BigDecimal renewalPrice;
    private String currencyCode;
    private String recurringId;
    private LocalDateTime timestamp;
    private List<MemberStatusAction> memberStatusActions;
    private MemberAccount memberAccount;

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public MembershipBuilder setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public BigDecimal getRenewalPrice() {
        return renewalPrice;
    }

    public MembershipBuilder setRenewalPrice(BigDecimal renewalPrice) {
        this.renewalPrice = renewalPrice;
        return this;
    }

    public String getCurrencyCode() {
        return this.currencyCode;
    }

    public MembershipBuilder setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public ProductStatus getProductStatus() {
        return productStatus;
    }

    public MembershipBuilder setProductStatus(ProductStatus productStatus) {
        this.productStatus = productStatus;
        return this;
    }

    public Long getId() {
        return id;
    }

    public MembershipBuilder setId(Long id) {
        this.id = id;
        return this;
    }

    public String getWebsite() {
        return website;
    }

    public MembershipBuilder setWebsite(String website) {
        this.website = website;
        return this;
    }

    public MemberStatus getStatus() {
        return status;
    }

    public MembershipBuilder setStatus(MemberStatus status) {
        this.status = status;
        return this;
    }

    public MembershipRenewal getMembershipRenewal() {
        return membershipRenewal;
    }

    public MembershipBuilder setMembershipRenewal(MembershipRenewal membershipRenewal) {
        this.membershipRenewal = membershipRenewal;
        return this;
    }

    public LocalDateTime getActivationDate() {
        return activationDate;
    }

    public MembershipBuilder setActivationDate(LocalDateTime activationDate) {
        this.activationDate = activationDate;
        return this;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public MembershipBuilder setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public Boolean getBookingLimitReached() {
        return isBookingLimitReached;
    }

    public MembershipBuilder setBookingLimitReached(Boolean bookingLimitReached) {
        isBookingLimitReached = bookingLimitReached;
        return this;
    }


    public long getMemberAccountId() {
        return memberAccountId;
    }

    public MembershipBuilder setMemberAccountId(long memberAccountId) {
        this.memberAccountId = memberAccountId;
        return this;
    }

    public BigDecimal getBalance() {
        return (BigDecimal) ObjectUtils.defaultIfNull(balance, BigDecimal.ZERO);
    }

    public MembershipBuilder setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public MembershipType getMembershipType() {
        return (MembershipType) ObjectUtils.defaultIfNull(membershipType, MembershipType.BASIC);
    }

    public MembershipBuilder setMembershipType(MembershipType membershipType) {
        this.membershipType = membershipType;
        return this;
    }

    public int getMonthsDuration() {
        return monthsDuration;
    }

    public MembershipBuilder setMonthsDuration(int monthsDuration) {
        this.monthsDuration = monthsDuration;
        return this;
    }

    public SourceType getSourceType() {
        return sourceType;
    }

    public MembershipBuilder setSourceType(SourceType sourceType) {
        this.sourceType = sourceType;
        return this;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public MembershipBuilder setRecurringId(String recurringId) {
        this.recurringId = recurringId;
        return this;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public MembershipBuilder setTimestamp(final LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public List<MemberStatusAction> getMemberStatusActions() {
        return memberStatusActions;
    }

    public MembershipBuilder setMemberStatusActions(List<MemberStatusAction> memberStatusActions) {
        this.memberStatusActions = memberStatusActions;
        return this;
    }

    public MemberAccount getMemberAccount() {
        return memberAccount;
    }

    public MembershipBuilder setMemberAccount(MemberAccount memberAccount) {
        this.memberAccount = memberAccount;
        return this;
    }

    public Membership build() {
        return new Membership(this);
    }

}
