package com.odigeo.membership.cookies;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

public class Cookie implements Serializable {

    private LocalDateTime expirationDate;

    private String nameAndValue;

    private String name;

    private String value;

    private String path;

    private String domain;

    private boolean isSecure;

    public LocalDateTime getExpirationDate() {
        return Optional.ofNullable(expirationDate)
                .map(date -> expirationDate)
                .orElse(null);
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = Optional.ofNullable(expirationDate)
                .map(date -> expirationDate)
                .orElse(null);
    }

    public String getNameAndValue() {
        return nameAndValue;
    }

    public void setNameAndValue(String nameAndValue) {
        this.nameAndValue = nameAndValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public boolean isSecure() {
        return isSecure;
    }

    public void setSecure(boolean secure) {
        isSecure = secure;
    }

    @Override
    public boolean equals(final Object o) {

        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final Cookie cookie = (Cookie) o;

        return isSecureEquals(cookie.isSecure)
                && isExpirationDateEquals(cookie.getExpirationDate())
                && isNameAndValueEquals(cookie.getNameAndValue())
                && isNameEquals(cookie.getName())
                && isValueEquals(cookie.getValue())
                && isPathEquals(cookie.path)
                && isDomainEquals(cookie.domain);
    }

    private boolean isSecureEquals(boolean isSecureCookie) {
        return isSecure == isSecureCookie;
    }

    private boolean isExpirationDateEquals(LocalDateTime localDateTimeCookie) {
        return (expirationDate != null ? expirationDate.equals(localDateTimeCookie) : localDateTimeCookie == null);
    }

    private boolean isNameAndValueEquals(String nameAndValueCookie) {
        return (nameAndValue != null ? nameAndValue.equals(nameAndValueCookie) : nameAndValueCookie == null);
    }

    private boolean isNameEquals(String nameCookie) {
        return (name != null ? name.equals(nameCookie) : nameCookie == null);
    }

    private boolean isValueEquals(String valueCookie) {
        return (value != null ? value.equals(valueCookie) : valueCookie == null);
    }

    private boolean isPathEquals(String pathCookie) {
        return (path != null ? path.equals(pathCookie) : pathCookie == null);
    }

    private boolean isDomainEquals(String domainCookie) {
        return (domain != null ? domain.equals(domainCookie) : domainCookie == null);
    }

    @Override
    public int hashCode() {
        int result = expirationDate != null ? expirationDate.hashCode() : 0;
        result = 31 * result + (nameAndValue != null ? nameAndValue.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        result = 31 * result + (domain != null ? domain.hashCode() : 0);
        result = 31 * result + (isSecure ? 1 : 0);
        return result;
    }
}
