package com.odigeo.membership.auth.exception;

import com.edreams.base.BaseException;

public class MembershipAuthServiceException extends BaseException {

    public MembershipAuthServiceException(String message) {
        this(message, (Throwable) null);
    }

    public MembershipAuthServiceException(String message, Throwable th) {
        super(message, th);
    }

}
