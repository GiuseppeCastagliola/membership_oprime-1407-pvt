package com.odigeo.membership.auth;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

public class LoginRequestTest extends BeanTest<LoginRequest> {
    @Test
    public void loginRequestEqualsVerifierTest() {
        EqualsVerifier.forClass(LoginRequest.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }

    @Override
    protected LoginRequest getBean() {
        return assembleLoginRequest();
    }

    private LoginRequest assembleLoginRequest() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setPassword("password");
        return loginRequest;
    }
}