package com.odigeo.membership;

import bean.test.BeanTest;

public class UpdateMemberAccountTest extends BeanTest<UpdateMemberAccount> {

    private static final long USER_ID = 123L;

    @Override
    protected UpdateMemberAccount getBean() {
        UpdateMemberAccount updateMemberAccount = new UpdateMemberAccount();
        updateMemberAccount.setUserId(USER_ID);
        return updateMemberAccount;
    }
}
