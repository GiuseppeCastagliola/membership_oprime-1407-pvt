Feature: Old model booking tracking

  Scenario Outline: Should update balance and track booking. Prime booking in contract, not tracked, and booking fees balanced
    Given the next booking detail retrieved by the API:
      | bookingId   | hasPerks | memberId   | bookingStatus | avoidFeeAmount    | perksFeeAmount    | creationDate   |
      | <bookingId> | true     | <memberId> | CONTRACT      | <bookingAvoidFee> | <bookingPerksFee> | <creationDate> |
    And the next membership stored in db:
      | memberId   | status             | balance         | activationDate   | expirationDate | memberAccountId | autoRenewal |
      | <memberId> | <membershipStatus> | <balanceBefore> | <activationDate> | 2019-10-10     | 1               | ENABLED     |
    When the booking with the ID <bookingId> is tracked
    Then the tracked booking exists in db
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount    | costFeeAmount | perksFeeAmount    |
      | <bookingId> | <memberId> | <creationDate> | <bookingAvoidFee> | 0.0           | <bookingPerksFee> |
    And the stored membership with ID <memberId> now has <balanceAfter> as balance
    Examples:
      | bookingId | memberId | membershipStatus | activationDate | creationDate | balanceBefore | balanceAfter | bookingAvoidFee | bookingPerksFee |
      | 66330     | 55440    | ACTIVATED        | 2018-10-10     | 2018-01-01   | 60.0          | 41.5         | -18.5           | -10.0           |
      | 66331     | 55441    | ACTIVATED        | 2018-10-10     | 2018-01-01   | 10.0          | -8.5         | -18.5           | -10.0           |

  Scenario Outline: Should track again and update the correct balance. Prime booking in contract, already tracked and fees amount changed
    Given the next booking detail retrieved by the API:
      | bookingId   | hasPerks | memberId   | bookingStatus | avoidFeeAmount    | perksFeeAmount    | creationDate   |
      | <bookingId> | true     | <memberId> | CONTRACT      | <bookingAvoidFee> | <bookingPerksFee> | <creationDate> |
    And the next membership stored in db:
      | memberId   | status             | balance         | activationDate | expirationDate | memberAccountId | autoRenewal |
      | <memberId> | <membershipStatus> | <balanceBefore> | 2018-10-10     | 2019-02-10     | 1               | ENABLED     |
    And the next tracked booking stored in db:
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount    | costFeeAmount | perksFeeAmount |
      | <bookingId> | <memberId> | <bookingDate>  | <trackedAvoidFee> | 0.0           | 10.0           |
    When the booking with the ID <bookingId> is tracked
    Then the tracked booking exists in db
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount    | costFeeAmount | perksFeeAmount    |
      | <bookingId> | <memberId> | <creationDate> | <bookingAvoidFee> | 0.0           | <bookingPerksFee> |
    And the stored membership with ID <memberId> now has <balanceAfter> as balance
    Examples:
      | bookingId | memberId | membershipStatus | bookingDate | creationDate | trackedAvoidFee | balanceBefore | bookingAvoidFee | bookingPerksFee | balanceAfter |
      | 66330     | 55440    | ACTIVATED        | 2018-01-02  | 2018-01-01   | -20.0           | 40.0          | -1.0            | -4.3            | 39.0         |
      | 66331     | 55441    | ACTIVATED        | 2018-02-02  | 2018-01-02   | -10.0           | 40.0          | -1.0            | -6.5            | 39.0         |
      | 66332     | 55442    | ACTIVATED        | 2018-03-02  | 2018-01-03   | -5.0            | -20.0         | -12.0           | -8.7            | -32.0        |
      | 66333     | 55443    | ACTIVATED        | 2018-04-02  | 2018-01-04   | 0.0             | -20.0         | -5.0            | -10.4           | -25.0        |

  Scenario Outline: Should remove booking tracking. Prime booking already tracked but not in contract
    Given the next booking detail retrieved by the API:
      | bookingId   | hasPerks | memberId   | bookingStatus | avoidFeeAmount    | perksFeeAmount    | creationDate   |
      | <bookingId> | true     | <memberId> | INIT          | <bookingAvoidFee> | <bookingPerksFee> | <creationDate> |
    And the next membership stored in db:
      | memberId   | status             | balance | activationDate | expirationDate | memberAccountId | autoRenewal |
      | <memberId> | <membershipStatus> | 40      | 2018-10-10     | 2019-02-10     | 1               | ENABLED     |
    And the next tracked booking stored in db:
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount | costFeeAmount | perksFeeAmount |
      | <bookingId> | <memberId> | <bookingDate>  | 10             | 0.0           | 20.0           |
    When the booking with the ID <bookingId> is tracked
    Then the tracked booking with the booking ID <bookingId> does not exist
    Examples:
      | bookingId | memberId | membershipStatus | bookingDate | creationDate | bookingAvoidFee | bookingPerksFee |
      | 66330     | 55440    | ACTIVATED        | 2018-01-02  | 2018-01-01   | -1.0            | -4.3            |

  Scenario Outline: Should keep balance and delete tracked booking. Prime booking already tracked but not in contract
    Given the next booking detail retrieved by the API:
      | bookingId   | hasPerks | memberId   | bookingStatus   |
      | <bookingId> | true     | <memberId> | <bookingStatus> |
    And the next membership stored in db:
      | memberId   | status             | activationDate | expirationDate | memberAccountId | autoRenewal | balance |
      | <memberId> | <membershipStatus> | 2018-10-10     | 2018-10-10     | 1               | ENABLED     | 54.99   |
    And the next tracked booking stored in db:
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount | perksFeeAmount |
      | <bookingId> | <memberId> | <bookingDate>  | -20.5          | -10.5          |
    When the booking with the ID <bookingId> is tracked
    Then the tracked booking with the booking ID <bookingId> does not exist
    And the stored membership with ID <memberId> now has 54.99 as balance
    Examples:
      | bookingId | memberId | membershipStatus | bookingStatus | bookingDate |
      | 1234      | 2990     | ACTIVATED        | INIT          | 2018-01-01  |
      | 1245      | 2991     | ACTIVATED        | REQUEST       | 2018-02-02  |
      | 1256      | 2992     | ACTIVATED        | HOLD          | 2018-03-03  |
      | 1267      | 2993     | ACTIVATED        | DIDNOTBUY     | 2018-04-04  |
      | 2055      | 2884     | DEACTIVATED      | PENDING       | 2018-05-15  |
      | 2056      | 2885     | DEACTIVATED      | RETAINED      | 2018-06-16  |
      | 2057      | 2886     | DEACTIVATED      | UNKNOWN       | 2018-06-17  |
      | 2058      | 2887     | DEACTIVATED      | FINAL_RET     | 2018-08-18  |
      | 3031      | 4040     | EXPIRED          | INIT          | 2018-01-21  |
      | 3032      | 4041     | EXPIRED          | REQUEST       | 2018-02-22  |
      | 3036      | 4045     | EXPIRED          | RETAINED      | 2018-06-26  |
      | 3038      | 4047     | EXPIRED          | FINAL_RET     | 2018-08-28  |