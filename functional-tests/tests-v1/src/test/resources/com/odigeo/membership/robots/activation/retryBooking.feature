Feature: Test retry of membership activation by booking id

  Background:
    Given Visit Engine responds the following test assignment like:
      | testName          | winnerPartition | enabled |
      | ULC_OPRIME556     | 2               | true    |

  Scenario Outline: Processing booking of PENDING_TO_ACTIVATE membership changes its status to ACTIVATED and sends correct subscription message
    Given the next memberAccount stored in db:
      | memberAccountId   | userId | firstName | lastNames |
      | <memberAccountId> | 1010   | JOSE      | GARCIA    |
    And the next membership stored in db:
      | memberId       | website | status         | autoRenewal | memberAccountId   | activationDate | expirationDate | balance |
      | <membershipId> | ES      | <statusBefore> | ENABLED     | <memberAccountId> | 2019-10-06     | 2020-10-06     | 0       |
    And property to send IDs to Kafka has value true in db:
    Given the next booking detail retrieved by the API:
      | bookingId   | hasPerks | memberId       | bookingStatus | testBooking | creationDate | avoidFeeAmount    | costFeeAmount    | perksFeeAmount| productType             | website | email                 |
      | <bookingId> | true     | <membershipId> | CONTRACT      | false       | 2019-10-06   | 0                 | 0                | 0             | MEMBERSHIP_SUBSCRIPTION | ES      | jose.garcia@gmail.com |
    And the user-api has the following user info
      | userId   | status       | email   | brand | token                    | tokenType        |
      | <userId> | <userStatus> | <email> | ED    | <passwordTokenGenerated> | REQUEST_PASSWORD |
    When requested to retry bookingId <bookingId>
    Then booking processing succeeds
    And membership <membershipId> has status <statusAfter>
    And if apply to <shouldSendMembershipIdsToKafka> membershipSubscriptionMessage is correctly sent to kafka queue and email will be sent to <email>
    And the message with member info is sent to kafka with shouldSetPassword <shouldSetPassword> and passwordToken <passwordToken> if apply <shouldSendMembershipIdsToKafka>
    Examples:
      | membershipId | memberAccountId | bookingId | statusBefore        | statusAfter | shouldSendMembershipIdsToKafka | userId  | email                 | userStatus    | passwordTokenGenerated | shouldSetPassword | passwordToken  |
      | 5555         | 387             | 10208     | PENDING_TO_ACTIVATE | ACTIVATED   | true                           | 1010    | jose.garcia@gmail.com | PENDING_LOGIN | passwordTokenA         | true              | passwordTokenA |
      | 5555         | 387             | 10208     | PENDING_TO_ACTIVATE | ACTIVATED   | true                           | 1010    | jose.garcia@gmail.com | ACTIVE        | passwordTokenB         | false             | null           |
      | 5555         | 387             | 10208     | PENDING_TO_ACTIVATE | ACTIVATED   | true                           | 1010    | jose.garcia@gmail.com | PENDING_LOGIN | null                   | false             | null           |
      | 5465         | 654             | 10154     | DEACTIVATED         | DEACTIVATED | false                          | 1010    | jose.garcia@gmail.com | PENDING_LOGIN | null                   | false             | null           |
      | 5466         | 655             | 10155     | EXPIRED             | EXPIRED     | false                          | 1010    | jose.garcia@gmail.com | PENDING_LOGIN | null                   | false             | null           |
      | 5467         | 656             | 10156     | UNKNOWN             | UNKNOWN     | false                          | 1010    | jose.garcia@gmail.com | PENDING_LOGIN | null                   | false             | null           |

  Scenario Outline: At successful membership activation a tracking is recorded
    Given the next memberAccount stored in db:
      | memberAccountId   | userId | firstName | lastNames |
      | <memberAccountId> | 1010   | JONATHAN  | CLINT     |
    And the next membership stored in db:
      | memberId       | website | status         | autoRenewal | memberAccountId   | activationDate | expirationDate | balance |
      | <membershipId> | OPFR    | <statusBefore> | ENABLED     | <memberAccountId> | <creationDate> | 2020-10-06     | 0       |
    And the next booking detail retrieved by the API:
      | bookingId   | hasPerks | memberId       | bookingStatus | testBooking | avoidFeeAmount    | costFeeAmount    | perksFeeAmount    | creationDate   | productType             | website | email                    |
      | <bookingId> | true     | <membershipId> | CONTRACT      | false       | <bookingAvoidFee> | <bookingCostFee> | <bookingPerksFee> | <creationDate> | MEMBERSHIP_SUBSCRIPTION | OPFR    | jonathan.clint@gmail.com |
    And the tracked booking with the booking ID <bookingId> does not exist
    And the user-api has the following user info
      | userId   | status       | email   | brand | token                    | tokenType        |
      | <userId> | <userStatus> | <email> | OP    | <passwordTokenGenerated> | REQUEST_PASSWORD |
    When requested to retry bookingId <bookingId>
    Then membership <membershipId> has status <statusAfter>
    And after some waiting the tracked booking exists in db
      | bookingId   | memberId       | bookingDateIso | avoidFeeAmount    | costFeeAmount    | perksFeeAmount    |
      | <bookingId> | <membershipId> | <creationDate> | <bookingAvoidFee> | <bookingCostFee> | <bookingPerksFee> |
    And membershipSubscriptionMessage is correctly sent to kafka queue and email will be sent to <email>
    And the message with shouldSetPassword <shouldSetPassword> and passwordToken <passwordToken> is sent to kafka
    Examples:
      | membershipId | memberAccountId | bookingId | statusBefore        | statusAfter | creationDate | bookingAvoidFee | bookingCostFee | bookingPerksFee | userId  | email                    | userStatus    | passwordTokenGenerated | shouldSetPassword | passwordToken  |
      | 5656         | 826             | 20397     | PENDING_TO_ACTIVATE | ACTIVATED   | 2019-10-06   | 0               | 0              | 0               | 1010    | jonathan.clint@gmail.com | PENDING_LOGIN | passwordTokenA         | true              | passwordTokenA |
      | 5656         | 826             | 20397     | PENDING_TO_ACTIVATE | ACTIVATED   | 2019-10-06   | 0               | 0              | 0               | 1010    | jonathan.clint@gmail.com | ACTIVE        | passwordTokenB         | false             | null           |
      | 5656         | 826             | 20397     | PENDING_TO_ACTIVATE | ACTIVATED   | 2019-10-06   | 0               | 0              | 0               | 1010    | jonathan.clint@gmail.com | PENDING_LOGIN | null                   | false             | null           |
