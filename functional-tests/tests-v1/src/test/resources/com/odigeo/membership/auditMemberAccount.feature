Feature: Audit member accounts
  Register into the audit member account table when a new account is created or modified

  Scenario Outline: Insert new member account is audited
    Given the following createNewMembershipRequest:
      | website   | monthsToRenewal | sourceType     | membershipType | userId   | name   | lastNames   |
      | <website> | 12              | FUNNEL_BOOKING | BASIC          | <userId> | <name> | <lastNames> |
    When the client request createMembership to membership API
    Then the member creation is audited with this information
    Examples:
      | userId | name | lastNames | website |
      | 777    | JOSE | PEREZ     | ES      |
      | 666    | LARA | MALDONADO | IT      |

  Scenario Outline: Update member account names is audited
    Given the next memberAccount stored in db:
      | memberAccountId   | userId   | firstName | lastNames     |
      | <memberAccountId> | <userId> | <oldName> | <oldLastName> |
    When the user wants to update the MemberAccount:
      | memberAccountId   | userId   | name      | lastNames     | operation    |
      | <memberAccountId> | <userId> | <newName> | <newLastName> | UPDATE_NAMES |
    Then the audit member account <memberAccountId> has userId <userId>, the name <newName> and the lastName <newLastName>
    Examples:
      | memberAccountId | userId | oldName | oldLastName | newName | newLastName |
      | 123             | 4321   | JOSE    | GARCIA      | PEPE    | GARCILASO   |
      | 993             | 1231   | MARIA   | SANCHEZ     | MARISA  | SAENZ       |

  Scenario Outline: Update member account userId is audited
    Given the next memberAccount stored in db:
      | memberAccountId   | userId          | firstName | lastNames  |
      | <memberAccountId> | <currentUserId> | <name>    | <lastName> |
    And the next membership stored in db:
      | memberId | website     | status             | autoRenewal | memberAccountId   | activationDate | expirationDate |
      | 4451     | ES          | ACTIVATED          | ENABLED     | <memberAccountId> | 2017-07-06     | 2018-07-06     |
    And the user-api has the following user info
      | userId          | status | email               | brand | token | tokenType        |
      | <currentUserId> | ACTIVE | <currentUsersEmail> | ED    | null  | REQUEST_PASSWORD |
      | <newUserId>     | ACTIVE | <newUsersEmail>     | ED    | null  | REQUEST_PASSWORD |
    When the user wants to update the MemberAccount:
      | memberAccountId   | userId      | operation      |
      | <memberAccountId> | <newUserId> | UPDATE_USER_ID |
    Then the audit member account <memberAccountId> has userId <newUserId>, the name <name> and the lastName <lastName>
    Examples:
      | memberAccountId | currentUserId | currentUsersEmail | newUserId | newUsersEmail       | name | lastName |
      | 5511            | 551000        | joseg@odigeo.com  | 5000155   | newuser1@odigeo.com | JOSE | GARCIA   |
      | 1938            | 193800        | anas@odigeo.com   | 8008391   | newuser2@odigeo.com | ANA  | SANCHEZ  |

  Scenario: All the member account changes are saved
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 99887766        | 112233 | LUIS      | RAMOS     |
    And the next membership stored in db:
      | memberId | website     | status             | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 4451     | ES          | ACTIVATED          | ENABLED     | 99887766        | 2017-07-06     | 2018-07-06     |
    And the user-api has the following user info
      | userId | status | email             | brand | token | tokenType        |
      | 112233 | ACTIVE | email1@odigeo.com | ED    | null  | REQUEST_PASSWORD |
      | 445566 | ACTIVE | email2@odigeo.com | ED    | null  | REQUEST_PASSWORD |
    When the user wants to update the MemberAccount:
      | memberAccountId | userId | name  | lastNames | operation      |
      | 99887766        | 112233 | FRANK | RAMOS     | UPDATE_NAMES   |
      | 99887766        | 112233 | FRAN  | CUERDA    | UPDATE_NAMES   |
      | 99887766        | 445566 | FRAN  | CUERDA    | UPDATE_USER_ID |
    Then all the member account 99887766 changes are audited
      | memberAccountId | userId | firstName | lastName |
      | 99887766        | 112233 | FRANK     | RAMOS    |
      | 99887766        | 112233 | FRAN      | CUERDA   |
      | 99887766        | 445566 | FRAN      | CUERDA   |
