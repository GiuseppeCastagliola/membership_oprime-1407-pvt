package com.odigeo.membership;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.odigeo.commons.test.docker.Artifact;
import com.odigeo.commons.test.docker.ContainerInfoBuilderFactory;
import com.odigeo.commons.test.docker.LogFilesTransfer;
import com.odigeo.membership.functionals.InstallerTestSuiteEnvironment;
import com.odigeo.membership.functionals.config.KafkaConfig;
import com.odigeo.membership.functionals.config.redis.RedisConfig;
import com.odigeo.technology.docker.ContainerComposer;
import com.odigeo.technology.docker.ContainerException;
import com.odigeo.technology.docker.ContainerInfo;
import com.odigeo.technology.docker.ContainerInfoBuilder;
import com.odigeo.technology.docker.DockerModule;
import com.odigeo.technology.docker.ImageController;
import com.odigeo.technology.docker.ImageException;
import com.odigeo.technology.docker.SocketPingChecker;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Guice;

import java.io.IOException;

@CucumberOptions(plugin = {"pretty", "json:target/cucumber/cucumber.json", "html:target/cucumber/"}, strict = true)
@Guice(modules = DockerModule.class)
public class BaseMembershipDockerTest extends AbstractTestNGCucumberTests {
    private static final Logger LOGGER = Logger.getLogger(BaseMembershipDockerTest.class);
    private static final String GROUP_ID = "com.odigeo.membership";
    private static final String MODULE_NAME = "membership";

    private final Artifact artifact = new Artifact(GROUP_ID, MODULE_NAME, System.getProperty("project.parent.version"));
    @Inject
    private ContainerComposer containerComposer;
    @Inject
    private ContainerInfoBuilderFactory containerInfoBuilderFactory;
    @Inject
    private ImageController imageController;
    @Inject
    private LogFilesTransfer logFilesTransfer;

    private InstallerTestSuiteEnvironment installerTestSuiteEnvironment;

    @BeforeClass
    public void setUp() throws IOException, ImageException, ContainerException {
        try {
            ConfigurationEngine.init();
            installerTestSuiteEnvironment = ConfigurationEngine.getInstance(InstallerTestSuiteEnvironment.class);
            ContainerInfo kafkaContainer = installKafka();
            ContainerInfo redisContainer = installRedis();
            ContainerInfo moduleContainer = containerInfoBuilderFactory.newModuleWithEmptyOracle(artifact)
                    .build();
            containerComposer.addService(kafkaContainer);
            containerComposer.addService(redisContainer);
            containerComposer.addServiceAndDependencies(moduleContainer, 50, 5000).composeUp(true);
            installerTestSuiteEnvironment.install();
            LOGGER.info("--------------- Starting integration tests ---------------");
        } catch (ContainerException | ImageException e) {
            LOGGER.error("error:", e);
            logFilesTransfer.copyToExternalFolder(artifact);
            throw e;
        } catch (IllegalArgumentException e) {
            LOGGER.error("error:", e);
            throw e;
        }
    }

    private ContainerInfo installKafka() throws ContainerException {
        KafkaConfig kafkaConfig = ConfigurationEngine.getInstance(KafkaConfig.class);
        String kafkaLocalIp = kafkaConfig.getLocalIp();
        SocketPingChecker socketPingChecker = new SocketPingChecker(kafkaLocalIp, kafkaConfig.getPort());
        return new ContainerInfoBuilder("spotify/kafka")
                .setName("kafka")
                .addPortMapping(kafkaConfig.getPort(), kafkaConfig.getPort())
                .addPortMapping(kafkaConfig.getZooKeeperPort(), kafkaConfig.getZooKeeperPort())
                .setPingChecker(socketPingChecker)
                .addEvironmentVariable("ADVERTISED_HOST", kafkaLocalIp)
                .addEvironmentVariable("ADVERTISED_PORT", kafkaConfig.getPort().toString())
                .build();
    }

    private ContainerInfo installRedis() throws ContainerException {
        RedisConfig redisConfig = ConfigurationEngine.getInstance(RedisConfig.class);
        SocketPingChecker socketPingChecker = new SocketPingChecker(redisConfig.getLocalIp(), redisConfig.getPort());
        return new ContainerInfoBuilder("redis:5.0.7-alpine")
                .setName("redis")
                .addPortMapping(redisConfig.getPort(), redisConfig.getPort())
                .setPingChecker(socketPingChecker)
                .build();
    }

    @AfterClass
    public void tearDown() throws IOException, ContainerException, ImageException {
        installerTestSuiteEnvironment.uninstall();
        logFilesTransfer.copyToExternalFolder(artifact);
        containerComposer.composeDown();
        imageController.removeFuncionalTestsImage(artifact.getArtifactId());
    }

}
