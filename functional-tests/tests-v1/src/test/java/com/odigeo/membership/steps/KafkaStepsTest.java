package com.odigeo.membership.steps;

import com.odigeo.membership.functionals.database.DatabaseWorld;
import com.odigeo.membership.mocks.kafka.BookingUpdatesV1KafkaProducer;
import com.odigeo.membership.mocks.kafka.MembershipSubscriptionMessageKafkaConsumer;
import com.odigeo.membership.mocks.kafka.MembershipUpdateMessageKafkaConsumer;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import org.mockito.Mock;
import org.mockito.verification.VerificationMode;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class KafkaStepsTest {

    private static final String EMAIL_TEST = "test@test.com";
    private static final VerificationMode ONCE = times(1);

    @Mock
    private MembershipSubscriptionMessageKafkaConsumer subscriptionConsumer;

    @Mock
    private MembershipUpdateMessageKafkaConsumer membershipUpdateMessageKafkaConsumer;
    
    @Mock
    private BookingUpdatesV1KafkaProducer kafkaProducer;

    @Mock
    private DatabaseWorld databaseWorld;

    @Mock
    private MembershipSubscriptionMessage membershipSubscriptionMessage;

    private KafkaSteps instance;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        this.instance = new KafkaSteps(this.databaseWorld, this.subscriptionConsumer, this.membershipUpdateMessageKafkaConsumer, this.kafkaProducer);
        List<MembershipSubscriptionMessage> membershipSubscriptionMessages = createSubscriptionMessagesList();
        when(subscriptionConsumer.getMessagesReceived()).thenReturn(membershipSubscriptionMessages);
    }

    @Test
    public void testReadingMessagesFromKafka() {
        this.instance.readMembershipSubscriptionMessageFromKafka(EMAIL_TEST);
        verify(this.subscriptionConsumer, ONCE).getMessagesReceived();
        verify(this.membershipSubscriptionMessage, ONCE).getSubscriptionStatus();
        verify(this.membershipSubscriptionMessage, ONCE).getEmail();
    }

    private List<MembershipSubscriptionMessage> createSubscriptionMessagesList() {
        when(this.membershipSubscriptionMessage.getSubscriptionStatus()).thenReturn(SubscriptionStatus.SUBSCRIBED);
        when(this.membershipSubscriptionMessage.getEmail()).thenReturn(EMAIL_TEST);
        return Collections.singletonList(this.membershipSubscriptionMessage);
    }

}
