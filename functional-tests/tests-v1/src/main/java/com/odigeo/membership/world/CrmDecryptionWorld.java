package com.odigeo.membership.world;

import cucumber.runtime.java.guice.ScenarioScoped;

import java.lang.reflect.UndeclaredThrowableException;

@ScenarioScoped
public class CrmDecryptionWorld {
    private String decryptedToken;
    private UndeclaredThrowableException crmException;

    public String getDecryptedToken() {
        return decryptedToken;
    }

    public void setDecryptedToken(String decryptedToken) {
        this.decryptedToken = decryptedToken;
    }

    public UndeclaredThrowableException getCrmException() {
        return crmException;
    }

    public void setCrmException(UndeclaredThrowableException crmException) {
        this.crmException = crmException;
    }
}
