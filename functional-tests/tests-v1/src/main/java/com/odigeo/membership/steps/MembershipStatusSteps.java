package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.converter.BooleanConverter;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.functionals.database.DatabaseWorld;
import com.odigeo.membership.functionals.membership.MembershipBuilder;
import com.odigeo.membership.response.MembershipInfo;
import com.odigeo.membership.world.MembershipManagementWorld;
import cucumber.api.Transform;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.commons.collections.CollectionUtils;
import org.testng.Assert;

import java.sql.SQLException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

@ScenarioScoped
public class MembershipStatusSteps extends CommonSteps {

    private final MembershipManagementWorld world;
    private final DatabaseWorld databaseWorld;

    @Inject
    public MembershipStatusSteps(MembershipManagementWorld world, ServerConfiguration serverConfiguration, DatabaseWorld databaseWorld) {
        super(serverConfiguration);
        this.world = world;
        this.databaseWorld = databaseWorld;
    }

    @Then("^membership (\\d+) has status (\\w+)$")
    public void membershipHasStatus(long membershipId, String status) {
        MembershipInfo newMembershipInfo = userAreaService.getMembershipInfo(membershipId, false);
        assertNotNull(newMembershipInfo);
        assertEquals(newMembershipInfo.getMembershipStatus(), status);
    }

    @Then("^membership with id (\\d+) is expired and a new activated is created$")
    public void previousMembershipBecomesExpiredAndNewIsCreated(Long membershipId) {
        membershipHasStatus(membershipId, "EXPIRED");
        membershipHasStatus(world.getCreatedMemberId(), "PENDING_TO_ACTIVATE");
    }


    @Then("^the deactived member with user_id (\\d+) in website (\\w+) is reactivated$")
    public void executeReactivateDeactivedMember(Long userId, String website) throws InvalidParametersException {
        assertEquals(world.getMembershipStatus().getStatus(), "ACTIVATED");
        assertNotNull(membershipService.getMembership(userId, website));
    }

    @When("^requested to reactivate member with memberId (\\d+)$")
    public void requestedToReactivateMember(Long memberId) throws InvalidParametersException {
        world.setMembershipStatus(membershipService.reactivateMembership(memberId));
    }

    @Then("^the reactivation of active member with user_id (\\d+) in website (\\w+) and status (\\w+) is not done$")
    public void executeReactivateActiveMember(Long userId, String website, String status) throws InvalidParametersException {
        assertEquals(status, world.getMembershipStatus().getStatus());
        assertNotNull(membershipService.getMembership(userId, website));
    }

    @Then("^the pending to activate member with user_id (\\d+) in website (\\w+) and status (\\w+) is not done$")
    public void executeReactivatePendingToActivateMember(Long userId, String website, String status) throws InvalidParametersException {
        assertEquals(status, world.getMembershipStatus().getStatus());
        assertNull(membershipService.getMembership(userId, website));
    }

    @When("^requested to reactivate fake member with (\\d+)$")
    public void requestedToReactivateFakeMember(Long memberId) {
        try {
            world.setMembershipStatus(membershipService.reactivateMembership(memberId));
        } catch (MembershipServiceException e) {
            world.addException(e);
        }
    }

    @When("^requested to disable member with memberId (\\d+)$")
    public void requestedToDisableMemberWithMemberId(Long memberId) {
        world.setMembershipStatus(membershipService.disableMembership(memberId));
    }

    @Then("^the userId (.*) and website (\\w+) is (\\w+)$")
    public void theMemberHasStatus(Long userId, String website, String expectedMemberStatus) {
        assertEquals(world.getMembershipStatus().getStatus(), expectedMemberStatus);
        assertNull(membershipService.getMembership(userId, website));
    }

    @Then("^the member is not disabled$")
    public void theMemberIsNotDisabled() {
        Assert.assertNotEquals(world.getMembershipStatus().getStatus(), "ACTIVATED");
    }

    @Then("^the result of the membership update operation is (true|false)$")
    public void theMembershipHasBeenCorrectlyUpdated(@Transform(BooleanConverter.class) boolean expectedResult) {
        assertEquals(world.isOperationSuccess(), expectedResult);
    }

    @Then("^a membership is created with the status (.*?)$")
    public void theMembershipIsCreatedWithTheCorrectStatus(String status) throws InterruptedException, SQLException, ClassNotFoundException {
        MembershipBuilder membershipBuilder = databaseWorld.getMembershipById(world.getCreatedMemberId());
        assertEquals(membershipBuilder.getStatus(), status);
    }
    @Then("^membership is created with autorenewal disabled$")
    public void theMembershipIsCreatedWithAutorenewalDisabled() throws InterruptedException, SQLException, ClassNotFoundException {
        MembershipBuilder membershipBuilder = databaseWorld.getMembershipById(world.getCreatedMemberId());
        assertEquals(membershipBuilder.getAutoRenewal(), "DISABLED");
    }

    @Then("^Exceptions occurs$")
    public void executeEnableWithException() {
        assertTrue(CollectionUtils.isNotEmpty(world.getExceptionList()));
    }
}
