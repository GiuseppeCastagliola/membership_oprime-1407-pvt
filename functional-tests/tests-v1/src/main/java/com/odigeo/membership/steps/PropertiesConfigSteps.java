package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.converter.BooleanConverter;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.functionals.database.DatabaseWorld;
import com.odigeo.membership.world.PropertiesConfigurationWorld;
import cucumber.api.Transform;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.sql.SQLException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@ScenarioScoped
public class PropertiesConfigSteps extends CommonSteps {

    private final PropertiesConfigurationWorld propertiesConfigWorld;
    private final DatabaseWorld databaseWorld;

    @Inject
    public PropertiesConfigSteps(final ServerConfiguration serverConfiguration, final PropertiesConfigurationWorld propertiesConfigWorld, final DatabaseWorld databaseWorld) {
        super(serverConfiguration);
        this.propertiesConfigWorld = propertiesConfigWorld;
        this.databaseWorld = databaseWorld;
    }

    @Given("^the configuration property (\\w+) stored in db with the value (true|false)$")
    public void theConfigurationPropertyStoredInDb(String property, @Transform(BooleanConverter.class) Boolean value) throws InterruptedException, SQLException, ClassNotFoundException {
        databaseWorld.addPropertiesConfigurationValue(property, value);
    }

    @When("^enabling the configuration for the property (\\w+)$")
    public void enablingTheConfigurationForTheProperty(String property) {
        propertiesConfigWorld.setUpdateResult(membershipPropertiesConfigService.enableConfigurationProperty(property));
    }

    @When("^disabling the configuration for the property (\\w+)$")
    public void disablingTheConfigurationForTheProperty(String property) {
        propertiesConfigWorld.setUpdateResult(membershipPropertiesConfigService.disableConfigurationProperty(property));
    }

    @Then("^the property was updated$")
    public void thePropertyWasUpdated() {
        assertTrue(propertiesConfigWorld.isUpdateResult());
    }

    @And("^the property (\\w+) has the value (true|false)$")
    public void thePropertyHasTheValueValueAfter(String property, @Transform(BooleanConverter.class) Boolean expectedResult) throws InterruptedException, SQLException, ClassNotFoundException {
        assertEquals(databaseWorld.findPropertiesConfigurationValue(property), expectedResult.booleanValue());
    }
}
