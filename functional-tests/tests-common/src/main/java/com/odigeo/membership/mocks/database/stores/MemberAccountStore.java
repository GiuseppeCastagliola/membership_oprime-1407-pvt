package com.odigeo.membership.mocks.database.stores;

import com.odigeo.membership.functionals.membership.MemberAccountBuilder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDateTime;
import java.util.List;

public class MemberAccountStore {

    private static final String SAVE_MEMBER = "INSERT INTO MEMBERSHIP_OWN.GE_MEMBER_ACCOUNT (ID, USER_ID, FIRST_NAME, LAST_NAME, TIMESTAMP) VALUES (?, ?, ?, ?, ?)";
    private static final String GET_MEMBER_ACCOUNT_BY_ID_SQL = "SELECT m.ID as ACCOUNT_ID,m.USER_ID,m.FIRST_NAME,m.LAST_NAME FROM GE_MEMBER_ACCOUNT m WHERE m.ID = ? ";
    private static final String USER_ID = "USER_ID";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String ACCOUNT_ID = "ACCOUNT_ID";

    public void saveMemberAccounts(Connection conn, List<MemberAccountBuilder> memberAccounts) throws SQLException {
        try (PreparedStatement pstmt = conn.prepareStatement(SAVE_MEMBER)) {

            for (MemberAccountBuilder memberAccountBuilder : memberAccounts) {
                int idx = 1;
                pstmt.setLong(idx++, memberAccountBuilder.getMemberAccountId());
                pstmt.setLong(idx++, memberAccountBuilder.getUserId());
                setStringOrNull(pstmt, memberAccountBuilder.getFirstName(), idx++);
                setStringOrNull(pstmt, memberAccountBuilder.getLastNames(), idx++);
                pstmt.setTimestamp(idx, Timestamp.valueOf(LocalDateTime.parse(memberAccountBuilder.getTimestamp())));
                pstmt.addBatch();
            }
            pstmt.executeBatch();
        }
    }

    private void setStringOrNull(PreparedStatement pstmt, String string, int idx) throws SQLException {
        if (string != null) {
            pstmt.setString(idx, string);
        } else {
            pstmt.setNull(idx, Types.VARCHAR);
        }
    }

    public MemberAccountBuilder getMemberAccountByMemberAccountId(Connection conn, long memberAccountId) throws SQLException {
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_MEMBER_ACCOUNT_BY_ID_SQL)) {
            preparedStatement.setLong(1, memberAccountId);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    return buildMemberAccount(rs);
                }
            }
        }
        return null;
    }

    private MemberAccountBuilder buildMemberAccount(ResultSet rs) throws SQLException {
        return new MemberAccountBuilder()
                .setFirstName(rs.getString(FIRST_NAME))
                .setLastNames(rs.getString(LAST_NAME))
                .setUserId(rs.getLong(USER_ID))
                .setMemberAccountId(rs.getLong(ACCOUNT_ID));
    }

}
