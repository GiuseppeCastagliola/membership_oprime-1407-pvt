package com.odigeo.membership.mocks.visitengine;

import com.google.inject.Inject;
import com.odigeo.membership.mocks.visitengine.request.TestAssignmentRequestTest;
import com.odigeo.membership.server.ServerStopException;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.List;
import java.util.Objects;

@ScenarioScoped
public class VisitEngineServiceWorld {

    private final VisitEngineServiceHttpServer visitEngineServiceHttpServer;
    private final TestAssignmentServiceMock testAssignmentServiceMock;

    @Inject
    public VisitEngineServiceWorld(VisitEngineServiceHttpServer visitEngineServiceHttpServer,
                                   TestAssignmentServiceMock testAssignmentServiceMock) {
        this.visitEngineServiceHttpServer = visitEngineServiceHttpServer;
        this.testAssignmentServiceMock = testAssignmentServiceMock;
    }

    public void install() throws ServerStopException {
        if (visitEngineServiceHttpServer.serverNotCreated()) {
            visitEngineServiceHttpServer.startServer();
        }
        visitEngineServiceHttpServer.addService(testAssignmentServiceMock);
    }

    public void uninstall() {
        if (Objects.nonNull(visitEngineServiceHttpServer)) {
            visitEngineServiceHttpServer.clearServices();
        }
    }

    public void addTestAssignmentBuilders(List<TestAssignmentRequestTest> testAssignmentRequestTests) {
        testAssignmentServiceMock.addTestAssignmentBuilders(testAssignmentRequestTests);
    }

}
