package com.odigeo.membership.mocks.database.stores;

import com.odigeo.membership.functionals.membership.AuditMemberAccountBuilder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AuditMemberAccountStore {

    private static final String FIND_MEMBER_ACCOUNT_AUDITS = "SELECT au.ID, au.MEMBER_ACCOUNT_ID, au.USER_ID, au.FIRST_NAME, au.LAST_NAME, au.TIMESTAMP FROM GE_AUDIT_MEMBER_ACCOUNT au WHERE au.MEMBER_ACCOUNT_ID = ? ORDER BY au.TIMESTAMP ASC";
    private static final String MEMBER_ACCOUNT_ID = "MEMBER_ACCOUNT_ID";
    private static final String TIMESTAMP = "TIMESTAMP";
    private static final String USER_ID = "USER_ID";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";

    public List<AuditMemberAccountBuilder> findMemberAccountAudits(final Connection conn, final Long memberAccountId) throws SQLException {
        List<AuditMemberAccountBuilder> audits = new ArrayList<>();
        try (PreparedStatement preparedStatement = conn.prepareStatement(FIND_MEMBER_ACCOUNT_AUDITS)) {
            preparedStatement.setLong(1, memberAccountId);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    audits.add(buildMemberAccountAudit(rs));
                }
            }
        }
        return audits;
    }

    private AuditMemberAccountBuilder buildMemberAccountAudit(final ResultSet rs) throws SQLException {
        return new AuditMemberAccountBuilder()
                .setMemberAccountId(rs.getLong(MEMBER_ACCOUNT_ID))
                .setUserId(rs.getLong(USER_ID))
                .setFirstName(rs.getString(FIRST_NAME))
                .setLastName(rs.getString(LAST_NAME))
                .setTimestamp(rs.getTimestamp(TIMESTAMP).toInstant());
    }
}
