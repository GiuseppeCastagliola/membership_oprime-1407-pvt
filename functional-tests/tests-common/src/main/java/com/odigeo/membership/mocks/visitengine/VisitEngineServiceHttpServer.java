package com.odigeo.membership.mocks.visitengine;

import com.google.inject.Singleton;
import com.odigeo.membership.server.JaxRsServiceHttpServer;

@Singleton
public class VisitEngineServiceHttpServer extends JaxRsServiceHttpServer {

    private static final String PATH = "/visit-engine";
    private static final int PORT = 57000;

    public VisitEngineServiceHttpServer() {
        super(PATH, PORT);
    }
}
